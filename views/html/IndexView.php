<!DOCTYPE html>
<html lang="fr">

  <head>
    <meta charset="utf-8" />
    <title>Accueil</title>
    <!-- CSS -->
    <link rel="stylesheet" href="./public/css/lib/bootstrap.min.css"/>
    <link rel="stylesheet" href="./public/css/lib/toastr.min.css"/>
    <link rel="stylesheet" href="./public/css/lib/all.css"/>
    <link rel="stylesheet" href="./public/css/lib/datatables.min.css"/>
    <link rel="stylesheet" href="./public/css/global.css"/>
    
    <!-- JS -->
    <script src="./public/js/lib/jquery-3.3.1.min.js"></script>
    <script src="./public/js/lib/popper.min.js"></script>
    <script src="./public/js/lib/bootstrap.min.js"></script>
    <script src="./public/js/lib/toastr.min.js"></script>
    <script src="./public/js/lib/datatables.min.js"></script>
    <script src="./public/js/userkanban.js"></script>
    <script src="./public/js/models/UserKanbanModel.js"></script>
    <script src="./public/js/views/UserKanbanView.js"></script>
  </head>
  <body>
    <div class="container">
      <?php
      require './views/html/NavbarView.php';
      if (isset($success)) {
        if ($success) {
          echo "<script>this.toastr.success('" . $message . "', 'Bravo');</script>";

        } else {
          echo "<script>this.toastr.error('" . $message . "', 'Erreur');</script>";
        }
      }
      ?>

      <div class="mt-2"> 
        <h3>Liste des Kanbans</h3>
        <table id="kanbans" class="table table-striped table-hover mt-3"> 
          <thead class='thead-dark'> 
            <tr> 
              <th class="w-40">Nom</th>
              <?php if ($isLogged) { ?>
              <th class="w-15">Role</th>
              <?php } ?>
              <th class="w-15">Gestionnaire</th>
              <th class="text-center w-15">Statut</th>
              <th class="text-right w-15">Actions</th>
            </tr> 
          </thead> 
          <tbody> 
            
          </tbody> 
        </table> 
      </div> 
    </div>

    <div id="modal-confirm" class="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Confirmation</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
        
					</div>
					<div class="modal-footer">
						<button type="button" id="yes-confirm-dialog" class="btn btn-dark">Oui</button>
						<button type="button" id="no-confirm-dialog" class="btn btn-dark" data-dismiss="modal">Non</button>
					</div>
				</div>
			</div>
		</div>
  </body>
</html>