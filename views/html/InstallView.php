<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Installation du projet</title>
    <meta charset="UTF-8" />
    <!-- CSS -->
    <link rel="stylesheet" href="./public/css/lib/bootstrap.min.css"/>
    <link rel="stylesheet" href="./public/css/lib/toastr.min.css"/>
    <link rel="stylesheet" href="./public/css/global.css"/>
    <link rel="stylesheet" href="./public/css/lib/all.css"/>
    <!-- JS -->
    <script src="./public/js/lib/jquery-3.3.1.min.js"></script>
    <script src="./public/js/lib/bootstrap.min.js"></script>
    <script src="./public/js/lib/toastr.min.js"></script>
</head>
<body>
    <?php
    if (isset($success)) {
        if ($success) {
            echo "<script>this.toastr.success('" . $message . "', 'Bravo');</script>";
            
        } else {
            echo "<script>this.toastr.error('" . $message . "', 'Erreur');</script>";
        }
    }
    ?>
    <div class="container mt-4">
        <h1 class="text-center mb-4">Configuration de la base de données</h1>
        <form method="post" action="install.php" class="offset-md-2 col-md-8">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-server"></i></div>
                </div>
                <input type="text" class="form-control" id="server"  name="server" placeholder="Serveur" required/>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-database"></i></div>
                </div>
                <input type="text" class="form-control" id="database" name="database" placeholder="Base de données" required/>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-user"></i></div>
                </div>
                <input type="text" class="form-control" id="username" name="username" placeholder="Nom d'utilisateur" required/>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-key"></i></div>
                </div>
                <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" required/>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="insert_data" name="insert_data" checked="checked" />
                <label class="form-check-label" for="insert_data">Initialiser avec des données</label>
            </div>
            <button type="submit" class="btn btn-dark float-right" name="install" value="install">Envoyer</button>
        </form>
    </div>
</body>
</html>
