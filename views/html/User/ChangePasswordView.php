<form method="post" action="user.php" class="offset-md-2 col-md-8">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-key"></i></div>
        </div>
        <input type="password" class="form-control" id="old_password"  name="old_password" placeholder="Ancien mot de passe" required/>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-key"></i></div>
        </div>
        <input type="password" class="form-control" id="new_password"  name="new_password" placeholder="Nouveau mot de passe" required/>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-key"></i></div>
        </div>
        <input type="password" class="form-control" id="confirm_password"  name="confirm_password" placeholder="Confirmer le mot de passe" required/>
    </div>
    <button type="submit" class="btn btn-dark float-right" name="type" value="changepassword">Changer le mot de passe</button>
</form>