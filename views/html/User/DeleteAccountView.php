<form method="post" action="user.php" class="offset-md-2 col-md-8">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-key"></i></div>
        </div>
        <input type="password" class="form-control" id="password"  name="password" placeholder="Mot de passe" required/>
    </div>
    <button type="submit" class="btn btn-danger float-right" name="type" value="deleteaccount">Supprimer le compte</button>
</form>