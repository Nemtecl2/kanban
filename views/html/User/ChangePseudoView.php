<form method="post" action="user.php" class="offset-md-2 col-md-8">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-user-tag"></i></div>
        </div>
        <input type="text" class="form-control" id="new_pseudo"  name="new_pseudo" placeholder="Nouveau pseudonyme" required/>
    </div>
    <button type="submit" class="btn btn-dark float-right" name="type" value="changepseudo">Changer le pseudonyme</button>
</form>