<nav class="nav nav-pills nav-fill mb-5">
    <a class="nav-item nav-link <?php if ($currentTab === 'changepseudo') {echo ' active';} ?>" href="user.php">Changer le pseudo</a>
    <a class="nav-item nav-link <?php if ($currentTab === 'changepassword') {echo ' active';} ?>" href="user.php?tab=changepassword">Changer le mot de passe</a>
    <a class="nav-item nav-link <?php if ($currentTab === 'deleteaccount') {echo ' active';} ?>" href="user.php?tab=deleteaccount">Supprimer le compte</a>
</nav>