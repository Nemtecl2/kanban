<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Modification de votre compte</title>
        <!-- CSS -->
        <link rel="stylesheet" href="./public/css/lib/bootstrap.min.css"/>
        <link rel="stylesheet" href="./public/css/lib/toastr.min.css"/>
        <link rel="stylesheet" href="./public/css/global.css"/>
        <link rel="stylesheet" href="./public/css/lib/all.css"/>
        <!-- JS -->
        <script src="./public/js/lib/jquery-3.3.1.min.js"></script>
        <script src="./public/js/lib/bootstrap.min.js"></script>
        <script src="./public/js/lib/toastr.min.js"></script>
    </head>
    <body>
        <div class="container">
            <?php
            require './views/html/NavbarView.php';
            if (isset($success)) {
                if ($success) {
                    echo "<script>this.toastr.success('" . $message . "', 'Bravo');</script>";

                } else {
                    echo "<script>this.toastr.error('" . $message . "', 'Erreur');</script>";
                }
            }
            require './views/html/User/UserNavbarView.php';
            switch ($currentTab) {
                case 'changepseudo':
                    require './views/html/User/ChangePseudoView.php';
                    break;
                case 'changepassword':
                    require './views/html/User/ChangePasswordView.php';
                    break;
                case 'deleteaccount':
                    require './views/html/User/DeleteAccountView.php';
                    break;
                default: 
                    break;
            }
            ?>
        </div>
    </body>
</html>