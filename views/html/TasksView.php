<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Tâches affectées</title>
        <!-- CSS -->
        <link rel="stylesheet" href="./public/css/lib/bootstrap.min.css"/>
        <link rel="stylesheet" href="./public/css/lib/toastr.min.css"/>
        <link rel="stylesheet" href="./public/css/lib/all.css"/>
        <link rel="stylesheet" href="./public/css/lib/datatables.min.css"/>
        <link rel="stylesheet" href="./public/css/global.css"/>

        <!-- JS -->
        <script src="./public/js/lib/jquery-3.3.1.min.js"></script>
        <script src="./public/js/lib/popper.min.js"></script>
        <script src="./public/js/lib/bootstrap.min.js"></script>
        <script src="./public/js/lib/toastr.min.js"></script>
        <script src="./public/js/lib/datatables.min.js"></script>
        <script src="./public/js/tasks.js"></script>
        <script src="./public/js/models/TaskModel.js"></script>
        <script src="./public/js/views/TasksView.js"></script>
    </head>
    <body>
        <div class="container">
            <?php
            require './views/html/NavbarView.php';
            ?>
            <div class="mt-2">
                <h3>Liste des tâches affectées</h3>
                <table id="tasks" class="table table-striped mt-3 table-hover">
                    <thead class='thead-dark'>
                    <tr>
                        <th>Kanban</th>
                        <th>Liste</th>
                        <th>Description</th>
                        <th>Date limite</th>
                        <th class="text-right w-15">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <div id="modal-confirm" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
            
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="yes-confirm-dialog" class="btn btn-dark">Oui</button>
                            <button type="button" id="no-confirm-dialog" class="btn btn-dark" data-dismiss="modal">Non</button>
                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>