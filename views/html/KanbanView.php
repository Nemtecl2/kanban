<!DOCTYPE html>
<html lang="fr">

  <head>
    <meta charset="utf-8" />
    <title>Kanban</title>
    <!-- CSS -->
    <link rel="stylesheet" href="./public/css/lib/bootstrap.min.css" />
    <link rel="stylesheet" href="./public/css/lib/toastr.min.css" />
    <link rel="stylesheet" href="./public/css/lib/select2.min.css"/>
    <link rel="stylesheet" href="./public/css/lib/dragula.min.css" />
    <link rel="stylesheet" href="./public/css/lib/all.css" />
    <link rel="stylesheet" href="./public/css/lib/tempusdominus.min.css" />
    <link rel="stylesheet" href="./public/css/lib/medium-editor.min.css" />
    <link rel="stylesheet" href="./public/css/lib/medium-editor-theme.min.css" />
    <link rel="stylesheet" href="./public/css/global.css" />
    <link rel="stylesheet" href="./public/css/kanban.css" />
    <!-- JS -->
    <script src="./public/js/lib/jquery-3.3.1.min.js"></script>
    <script src="./public/js/lib/popper.min.js"></script>
    <script src="./public/js/lib/bootstrap.min.js"></script>
    <script src="./public/js/lib/toastr.min.js"></script>
    <script src="./public/js/lib/select2.min.js"></script>
    <script src="./public/js/lib/dragula.min.js"></script>
    <script src="./public/js/lib/dom-autoscroller.js"></script>
    <script src="./public/js/lib/moment.min.js"></script>
    <script src="./public/js/lib/moment-fr.js"></script>
    <script src="./public/js/lib/tempusdominus.js"></script>
    <script src="./public/js/lib/medium-editor.min.js"></script>
    <script src="./public/js/models/KanbanModel.js"></script>
    <script src="./public/js/models/ListModel.js"></script>
    <script src="./public/js/models/TaskModel.js"></script>
    <script src="./public/js/views/KanbanView.js"></script>
    <script src="./public/js/kanban.js"></script>
  </head>

  <body>
    <div class="container">
      <input id="kanban-id" name="kanbanId" type="hidden" value="<?php echo $kanbanId; ?>" />
      <input id="user-id" name="userId" type="hidden" value="<?php echo $userId; ?>" />
      <?php
      require './views/html/NavbarView.php';
      ?>
      <div class="row">
        <div class="col-md-8">
          <a class='ml-2' data-toggle='tooltip' data-placement='top' title='Statut'>
            <i class='kanban-status fa fa-eye<?php echo ($status === 'private' ? '-slash' : ''); ?> fa-lg'></i>
          </a>
          <h3 class='kanban-title'><?php echo $name; ?></h3>
          <span class="user-rights">
            <a class='ml-2' data-toggle='tooltip' data-placement='top' title='Droits'>
              <i class="fas fa-id-card"></i> 
            </a> 
            <?php echo $rights; ?>
          </span>
        </div>
        <?php if ($canAdd) { ?>
        <div class="offset-md-1 col-md-3" id="add-line">
          <div class="col-md-12">
            <span class="float-right">
              <a class="text-dark" href="#add-users-modal" data-toggle="modal">Ajouter des membres<i class="fas fa-plus-circle ml-1"></i></a>
            </span>
          </div>
          <div class="col-md-12">
            <span class="float-right">
              <a class="text-dark" href="#add-lists-modal" data-toggle="modal">Ajouter des listes<i class="fas fa-plus-circle ml-1"></i></a>
            </span>
          </div>
        </div>
        <?php } ?>
      </div> 
      <div class="kanban-container" id="kanban-container">
      </div>
    </div>

    <?php if ($isManager) { ?>
    <!-- Modal ajout d'utilisateurs -->
    <div class="modal fade" id="add-users-modal">
      <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Ajouter des membres</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="container">
              <div class="input-group select2-bootstrap-prepend">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fa fa-users"></i></div>
                </div>
                <select id="add-users" class="form-control" name="users" multiple="multiple"></select>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
          <button id="cancel-adding-users" type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
          <button id="add-users-submit" type="button" class="btn btn-success" data-dismiss="modal">Ajouter</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal ajout de listes -->
    <div class="modal fade" id="add-lists-modal">
      <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Ajouter des listes</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="container">
              <div class="input-group select2-bootstrap-prepend">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fas fa-list-ul"></i></div>
                </div>
                <select id="add-lists" class="form-control" name="add-lists" multiple="multiple"></select>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button id="cancel-adding-lists" type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
            <button id="add-lists-submit" type="button" class="btn btn-success" data-dismiss="modal">Ajouter</button>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    
    <?php if ($isLogged) { ?>
    <!-- Modal ajout d'une carte -->
    <div class="modal fade" id="add-task-modal">
      <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Ajout d'une carte</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="container">
              <input type="hidden" id="add-task-list-id" />
              <span>Description</span>
              <div id="add-task-description" class="medium-editor mb-3"></div>
              <div class="input-group date mb-3" id="add-task-date-timepicker" data-target-input="nearest">
                <div class="input-group-prepend" data-target="#add-task-date-timepicker" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                </div>
                <input type="text" id="add-task-date" class="form-control datetimepicker-input" placeholder="Saisir une date" data-toggle="datetimepicker" data-target="#add-task-date-timepicker" autocomplete="off"/>
              </div>
              <div class="input-group select2-bootstrap-prepend mb-3">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fa fa-users"></i></div>
                </div>
                <select id="add-task-user" class="form-control" name="task-user"></select>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
          <button id="cancel-adding-task" type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
            <button id="add-task-submit" type="button" class="btn btn-success" data-dismiss="modal">Ajouter</button>
          </div>
        </div>
      </div>
    </div>
    <?php } ?> 

    <?php if ($isManager) { ?>
    <!-- Modal modification d'une liste -->
    <div class="modal fade" id="change-list-modal">
      <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Modification de la liste</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <input type="hidden" id="current-list-id" value="" />
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text"><i class="fas fa-list-ul"></i></div>
              </div>
              <input type="text" class="form-control" id="update-list-name" placeholder="Nouveau nom">
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
          <button id="delete-list" type="button" class="btn btn-danger" data-dismiss="modal">Supprimer</button>
          <button id="update-list-submit" type="button" class="btn btn-success" data-dismiss="modal">Sauvegarder</button>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>

    <?php if ($isLogged) { ?>
    <!-- Modal modification d'une carte -->
    <div class="modal fade" id="change-task-modal">
      <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Modification d'une carte</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
          <div class="container">
              <input type="hidden" id="change-task-id" />
              <span>Description</span>
              <div id="change-task-description" class="medium-editor mb-3"></div>
              <div class="input-group date mb-3" id="change-task-date-timepicker" data-target-input="nearest">
                <div class="input-group-prepend" data-target="#change-task-date-timepicker" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                </div>
                <input type="text" id="change-task-date" class="form-control datetimepicker-input" placeholder="Saisir une date" data-toggle="datetimepicker" data-target="#change-task-date-timepicker" autocomplete="off"/>
              </div>
              <div class="input-group select2-bootstrap-prepend mb-3">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fa fa-users"></i></div>
                </div>
                <select id="change-task-user" class="form-control" name="task-user"></select>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button id="delete-task" type="button" class="btn btn-danger" data-dismiss="modal">Supprimer</button>
            <button id="change-task-submit" type="button" class="btn btn-success" data-dismiss="modal">Sauvegarder</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal confirmation -->
    <div id="modalConfirm" class="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Confirmation</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
        
					</div>
					<div class="modal-footer">
						<button type="button" id="yesConfirmDialog" class="btn btn-custom btn-dark">Oui</button>
						<button type="button" id="noConfirmDialog" class="btn btn-custom btn-dark" data-dismiss="modal">Non</button>
					</div>
				</div>
			</div>
		</div>
    <?php } ?>
  </body>
</html>