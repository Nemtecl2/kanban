<!DOCTYPE html>
<html lang="fr">

  <head>
    <meta charset="utf-8" />
    <title>Page d'erreur</title>
    <!-- CSS -->
    <link rel="stylesheet" href="./public/css/lib/bootstrap.min.css"/>
    <link rel="stylesheet" href="./public/css/lib/toastr.min.css"/>
    <link rel="stylesheet" href="./public/css/global.css"/>
    <link rel="stylesheet" href="./public/css/lib/all.css"/>
    <!-- JS -->
    <script src="./public/js/lib/jquery-3.3.1.min.js"></script>
    <script src="./public/js/lib/bootstrap.min.js"></script>
    <script src="./public/js/lib/toastr.min.js"></script>
  </head>
  <body>
    <div class="container">
      <?php
      require './views/html/NavbarView.php';
      ?>
      <h2 class="mt-5 text-center"><?php echo $errorMessage; ?></h2>      
    </div>
  </body>
</html>