<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mt-2">
  <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse"
    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item<?php if ($currentPage === 'index') {echo ' active';} ?>">
        <a class="nav-link" href="index.php">Home</a>
      </li>
      <?php if ($isLogged) { ?>
      <li class="nav-item<?php if ($currentPage === 'newkanban') {echo ' active';} ?>">
        <a class="nav-link" href="newkanban.php">Nouveau tableau</a>
      </li>
      <li class="nav-item<?php if ($currentPage === 'tasks') {echo ' active';} ?>">
        <a class="nav-link" href="tasks.php">Tâches affectées</a>
      </li>
      <?php } ?>
    </ul>
    <ul class="nav navbar-nav flex-row justify-content-between ml-auto">
      <?php if (!$isLogged) { ?>
      <li class="nav-item<?php if ($currentPage === 'signup') {echo ' active';} ?>">
        <a class="nav-link" href="signup.php">S'inscrire</a>
      </li>
      <li class="nav-item<?php if ($currentPage === 'signin') {echo ' active';} ?>">
        <a class="nav-link" href="signin.php">Se connecter</a>
      </li>
      <?php } else { ?>
      <li class="nav-item <?php if ($currentPage === 'user') {echo ' active';} ?>">
        <a class="nav-link" href="user.php"><i class="fa fa-user mr-1"></i><?php echo $_SESSION['user']['Pseudo']; ?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="signout.php">Se déconnecter</a>
      </li>
      <?php } ?>
    </ul>
  </div>
</nav>