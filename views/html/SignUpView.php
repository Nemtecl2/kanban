<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>S'inscrire</title>
        <!-- CSS -->
        <link rel="stylesheet" href="./public/css/lib/bootstrap.min.css">
        <link rel="stylesheet" href="./public/css/global.css">
        <link rel="stylesheet" href="./public/css/lib/toastr.min.css">
        <link rel="stylesheet" href="./public/css/lib/all.css">
        <!-- JS  -->
        <script src="./public/js/lib/jquery-3.3.1.min.js"></script>
        <script src="./public/js/lib/bootstrap.min.js"></script>
        <script src="./public/js/lib/toastr.min.js"></script>
    </head>
    <body>
        <div class="container">
            <?php
            require './views/html/NavbarView.php';
            if (isset($success)) {
                if ($success) {
                    echo "<script>this.toastr.success('" . $message . "', 'Bravo');</script>";
                } else {
                    echo "<script>this.toastr.error('" . $message . "', 'Erreur');</script>";
                }
            }
            ?>
            <h1 class="text-center mb-4">Inscription</h1>
            <form method="post" action="signup.php" class="offset-md-2 col-md-8">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-user-tag"></i></div>
                    </div>
                    <input type="text" class="form-control" id="pseudo"  name="pseudo" placeholder="Pseudonyme" required/>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-user"></i></div>
                    </div>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Nom d'utilisateur" required/>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-key"></i></div>
                    </div>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" required/>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-key"></i></div>
                    </div>
                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirmer le mot de passe" required/>
                </div>
                <button type="submit" class="btn btn-dark float-right">S'inscrire</button>
            </form>
        </div>
    </body>
</html>