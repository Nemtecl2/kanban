<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Nouveau Kanban</title>
        <!-- CSS -->
        <link rel="stylesheet" href="./public/css/lib/bootstrap.min.css"/>
        <link rel="stylesheet" href="./public/css/lib/toastr.min.css"/>
        <link rel="stylesheet" href="./public/css/lib/select2.min.css"/>
        <link rel="stylesheet" href="./public/css/global.css"/>
        <link rel="stylesheet" href="./public/css/lib/all.css"/>

        <!-- JS -->
        <script src="./public/js/lib/jquery-3.3.1.min.js"></script>
        <script src="./public/js/lib/bootstrap.min.js"></script>
        <script src="./public/js/lib/toastr.min.js"></script>
        <script src="./public/js/lib/select2.min.js"></script>
        <script src="./public/js/models/UserModel.js"></script>
        <script src="./public/js/models/KanbanModel.js"></script>
        <script src="./public/js/views/NewKanbanView.js"></script>
        <script src="./public/js/newkanban.js"></script>
    </head>
    <body>
        <div class="container">
            <?php
            require './views/html/NavbarView.php';
            ?>
            <h1 class="text-center mb-4">Ajouter un kanban</h1>
            <form id="new-kanban-form" class="offset-md-2 col-md-8">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fab fa-trello"></i></div>
                    </div>
                    <input type="text" class="form-control" id="name"  name="name" placeholder="Nom" required/>
                </div>
                <div class="mb-3">
                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="status" id="private_status" value="private" checked>
                        <label class="form-check-label" for="private_status">
                            Privé
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="status" id="public_status" value="public">
                        <label class="form-check-label" for="public_status">
                            Public
                        </label>
                    </div>
                </div>
                <div class="input-group select2-bootstrap-prepend mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-users"></i></div>
                    </div>
                    <select id="user-list" class="form-control" name="users" multiple="multiple"></select>
                </div>
                <div class="input-group select2-bootstrap-prepend mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-list-ul"></i></div>
                    </div>
                    <select id="lists" class="form-control" name="lists" multiple="multiple"></select>
                </div>
                <button type="submit" class="btn btn-success float-right">Ajouter un kanban</button>
            </form>
        </div>
    </body>
</html>