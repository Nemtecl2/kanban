<?php
require_once($modelPath . 'Database.php');

/**
 * Modèle de la table User
 */
class UserModel {
    
    /**
     * Fonction qui initialise les tables de la base de données
     * @static
     */
    public static function initTables() {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        
        $createTables = file_get_contents($scriptPath . 'install/kanban.sql');
        $pdo->exec($createTables);

        $database = null;
    }
    
    /**
     * Fonction qui insert des données par défaut (depuis la case à cocher)
     * @static
     */
    public static function insertData() {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        
        $insert = file_get_contents($scriptPath . 'install/insert_data.sql');
        $pdo->exec($insert);

        $database = null;
    }
}

?>