<?php
require_once($modelPath . 'Database.php');

/**
 * Modèle de la table Kanban
 */
class KanbanModel {

    /**
     * Fonction qui sélectionne un kanban
     * @static
     * @param name nom du kanban
     * @returns le kanban souhaité
     */
    public static function selectKanban($name) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_kanban.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':name', $name);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne un kanban
     * @static
     * @param kanbanId identifiant du kanban
     * @returns le kanban souhaité
     */
    public static function selectKanbanWithId($kanbanId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_kanban_with_id.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':kanbanId', $kanbanId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne les listes et tâches d'un kanban
     * @static
     * @param kanbanId identifiant du kanban
     * @returns l'ensemble des informations souhaitées
     */
    public static function selectKanbanListTask($kanbanId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_kanban_list_task.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':kanbanId', $kanbanId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui insert un kanban
     * @static
     * @param managerId identifiant du gestionnaire du kanban
     * @param name nom du kanban
     * @param statut statut du kanban (public ou privé)
     */
    public static function insertKanban($managerId, $name, $status) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'insert/insert_kanban.sql');
        $insert = $pdo->prepare($script);
    
        $insert->bindParam(':managerId', $managerId);
        $insert->bindParam(':name', $name);
        $insert->bindParam(':status', $status);

        $insert->execute();

        $database = null;
    }

    /**
     * Fonction qui supprime un kanban
     * @static
     * @param name nom du kanban
     */
    public static function deleteKanban($name) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'delete/delete_kanban.sql');
        $delete = $pdo->prepare($script);

        $delete->bindParam(':name', $name);

        $delete->execute();

        $database = null;
    }
}
?>