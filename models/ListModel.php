<?php
require_once($modelPath . 'Database.php');

/**
 * Modèle de la table List
 */
class ListModel {

    /**
     * Fonction qui sélectionne les listes d'un kanban
     * @static
     * @param kanbanId identifiant du kanban
     * @returns les listes du kanban
     */
    public static function selectLists($kanbanId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_lists.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':kanbanId', $kanbanId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne les listes d'un kanban sauf une
     * @static
     * @param kanbanId identifiant du kanban
     * @param listId identifiant de la liste
     * @returns les listes souhaitées
     */
    public static function selectListsExceptOne($kanbanId, $listId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_lists_except_one.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':kanbanId', $kanbanId);
        $select->bindParam(':listId', $listId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui insert une liste
     * @static
     * @param name nom de la liste
     */
    public static function insertList($name) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');

        $pdo = $database->getPdo();

        $script = file_get_contents($scriptPath . 'insert/insert_list.sql');

        $insert = $pdo->prepare($script);
        $insert->bindParam(':name', $name);
        $insert->execute();

        $database = null;
    }

    /**
     * Fonction qui insert une liste dans un kanban
     * @static
     * @param kanbanId identifiant du kanban
     * @param name nom de la liste
     */
    public static function insertListInKanban($kanbanId, $name) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');

        $pdo = $database->getPdo();

        $script = file_get_contents($scriptPath . 'insert/insert_list_in_kanban.sql');

        $insert = $pdo->prepare($script);
        $insert->bindParam(':kanbanId', $kanbanId);
        $insert->bindParam(':name', $name);
        $insert->execute();

        $database = null;
    }

    /**
     * Fonction qui met à jour une liste
     * @static
     * @param listId identifiant de la liste
     * @param newName nouveau nom de la liste
     */
    public static function updateList($listId, $newName) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'update/update_list.sql');

        $update = $pdo->prepare($script);
        $update->bindParam(':listId', $listId);
        $update->bindParam(':name', $newName);

        $update->execute();

        $database = null;
    }

    /**
     * Fonction qui supprime une liste
     * @static
     * @param listId identifiant de la liste
     */
    public static function deleteList($listId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'delete/delete_list.sql');
        $delete = $pdo->prepare($script);

        $delete->bindParam(':listId', $listId);

        $delete->execute();

        $database = null;
    }
}
?>