<?php
require_once($modelPath . 'Database.php');

/**
 * Modèle de la table UserKanban
 */
class UserKanbanModel {

    /**
     * Fonction qui sélectionne les différents kanbans d'un utilisateur
     * @static
     * @param userId identifiant de l'utilisateur
     * @returns les kanbans souhaitées
     */
    public static function selectUserKanban($userId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_user_kanban.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':userId', $userId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne un kanban spécifique
     * @static
     * @param kanbanId identifiant du kanban
     * @param userId identifiant de l'utilisateur
     * @returns le kanban souhaité
     */
    public static function selectUserKanbanWithIds($kanbanId, $userId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_user_kanban_with_ids.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':kanbanId', $kanbanId);
        $select->bindParam(':userId', $userId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne les kanbans publics
     * @static
     * @returns l'ensemble des kanbans publics
     */
    public static function selectKanbanPublic() {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_kanban_public.sql');

        $select = $pdo->prepare($script);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne les utilisateurs d'un kanban
     * @static
     * @param kanbanId identifiant du kanban
     * @returns les utilisateurs du kanban
     */
    public static function selectUsersFromKanban($kanbanId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_users_from_kanban.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':kanbanId', $kanbanId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne le rôle de l'utilisateur au sein d'un kanban
     * @static
     * @param userId identifiant de l'utilisateur
     * @param kanbanId identifiant du kanban
     * @returns sélectionne le rôle de l'utilisateur au sein du kanban souhaité
     */
    public static function selectRole($userId, $kanbanId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_role.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':userId', $userId);
        $select->bindParam(':kanbanId', $kanbanId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui insert un invité
     * @static
     * @param userId identifiant de l'utilisateur
     */
    public static function insertGuest($userId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');

        $pdo = $database->getPdo();

        $script = file_get_contents($scriptPath . 'insert/insert_guest.sql');

        $insert = $pdo->prepare($script);
        $insert->bindParam(':userId', $userId);
        $insert->execute();

        $database = null;
    }

    /**
     * Fonction qui insert un invité dans un kanban
     * @static
     * @param kanbanId identifiant du kanban
     * @param userId identifiant de l'utilisateur
     */
    public static function insertGuestInKanban($kanbanId, $userId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');

        $pdo = $database->getPdo();

        $script = file_get_contents($scriptPath . 'insert/insert_guest_in_kanban.sql');

        $insert = $pdo->prepare($script);
        $insert->bindParam(':userId', $userId);
        $insert->bindParam(':kanbanId', $kanbanId);
        $insert->execute();

        $database = null;
    }
}
?>