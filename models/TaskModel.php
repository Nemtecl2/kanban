<?php
require_once($modelPath . 'Database.php');

/**
 * Modèle de la table Task
 */
class TaskModel {

    /**
     * Fonction qui sélectionne les tâches d'un utilisateur
     * @static
     * @param userId identifiant de l'utilisateur
     * @returns les tâches souhaitées
     */
    public static function selectTasks($userId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_tasks.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':userId', $userId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne une tâche
     * @static
     * @param userId identifiant de l'utilisateur
     * @param taskId identifiant de la tâche
     * @returns la tâche souhaitée
     */
    public static function selectTask($userId, $taskId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_task.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':userId', $userId);
        $select->bindParam(':taskId', $taskId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui insert une tâche
     * @static
     * @param listId identifiant de la liste
     * @param userId identifiant de l'utilisateur
     * @param description description de la tâche
     * @param date date limite de réalisation de la tâche
     */
    public static function insertTask($listId, $userId, $description, $date) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');

        $pdo = $database->getPdo();

        $script = file_get_contents($scriptPath . 'insert/insert_task.sql');

        $insert = $pdo->prepare($script);
        $insert->bindParam(':listId', $listId);
        $insert->bindParam(':userId', $userId);
        $insert->bindParam(':description', $description);
        $insert->bindParam(':date', $date);

        $insert->execute();

        $database = null;
    }

    /**
     * Fonction qui met à jour une tâche
     * @static
     * @param userId identifiant de l'utilisateur
     * @param taskId identifiant de la tâche
     * @param description description de la tâche
     * @param date date limite de réalisation de la tâche
     */
    public static function updateTask($userId, $taskId, $description, $date) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'update/update_task.sql');

        $update = $pdo->prepare($script);
        $update->bindParam(':taskId', $taskId);
        $update->bindParam(':userId', $userId);
        $update->bindParam(':description', $description);
        $update->bindParam(':date', $date);

        $update->execute();

        $database = null;
    }

    /**
     * Fonction qui met à jour une tâche
     * @static
     * @param taskId identifiant de la tâche
     * @param listId identifiant de la liste
     */
    public static function updateTaskList($taskId, $listId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'update/update_task_list.sql');

        $update = $pdo->prepare($script);
        $update->bindParam(':listId', $listId);
        $update->bindParam(':taskId', $taskId);

        $update->execute();

        $database = null;
    }

    /**
     * Fonction qui supprime une tâche
     * @static
     * @param taskId identifiant de la tâche
     */
    public static function deleteTask($taskId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'delete/delete_task.sql');
        $delete = $pdo->prepare($script);

        $delete->bindParam(':taskId', $taskId);

        $delete->execute();

        $database = null;
    }
}
?>