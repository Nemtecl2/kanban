<?php
/**
 * Classe permettant de communiquer avec la base de données
 */
class Database {
    private $pdo;

    /**
     * Constructeur
     * @param filePath chemin du fichier de configuration (config.txt)
     */
    public function __construct($filePath) {
        $contents = explode('-', file_get_contents($filePath));
        $dsn = $contents[0];
        $dbusr = $contents[1];
        $dbpasswd = $contents[2];

        $this->pdo = new PDO($dsn, $dbusr, $dbpasswd);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    /**
     * Fonction qui retourne le pdo
     * @returns pdo
     */
    public function getPdo() {
        return $this->pdo;
    }
}
?>