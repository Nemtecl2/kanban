<?php
require_once($modelPath . 'Database.php');

/**
 * Modèle de la table User
 */
class UserModel {

    /**
     * Fonction qui sélectionne un utilisateur
     * @static
     * @param username nom d'utilisateur
     * @returns l'utilisateur souhaité
     */
    public static function selectUser($username) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_user.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':username', $username);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne un utilisateur spécifique
     * @static
     * @param userId identifiant de l'utilisateur
     * @returns l'utilisateur souhaité
     */
    public static function selectUserWithId($userId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_user_with_id.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':userId', $userId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne tous les utilisateurs sauf un
     * @static
     * @param userId identifiant de l'utilisateur
     * @returns l'ensemble des utilisateurs souhaitées
     */
    public static function selectUsersExceptOne($userId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_users_except_one.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':userId', $userId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne la liste des utilisateurs d'un kanban
     * @static
     * @param kanbanId identifiant du kanban
     * @returns l'ensemble des utilisateurs souhaités
     */
    public static function selectAvailableUsers($kanbanId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_available_users.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':kanbanId', $kanbanId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui sélectionne les utilisateurs d'un kanban
     * @static
     * @param kanbanId identifiant du kanban
     * @returns l'ensemble des utilisateurs souhaités
     */
    public static function selectAvailableUsersByRole($kanbanId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'select/select_available_users_by_role.sql');

        $select = $pdo->prepare($script);
        $select->bindParam(':kanbanId', $kanbanId);

        $select->execute();

        $database = null;

        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fonction qui insert un utilisateur
     * @static
     * @param username nom d'utilisateur
     * @param passwd mot de passe de l'utilisateur
     * @param pseudo pseudo de l'utilisateur
     */
    public static function insertUser($username, $passwd, $pseudo) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
    
        $passwd = password_hash($passwd, PASSWORD_BCRYPT);
        $script = file_get_contents($scriptPath . 'insert/insert_user.sql');
        $insert = $pdo->prepare($script);
    
        $insert->bindParam(':username', $username);
        $insert->bindParam(':passwd', $passwd);
        $insert->bindParam(':pseudo', $pseudo);
    
        $insert->execute();

        $database = null;
    }

    /**
     * Fonction qui met à jour un utilisateur
     * @static
     * @param username nom d'utilisateur
     * @param newPseudo nouveau pseudo de l'utilisateur
     */
    public static function updatePseudo($username, $newPseudo) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'update/update_user_pseudo.sql');

        $update = $pdo->prepare($script);
        $update->bindParam(':username', $username);
        $update->bindParam(':pseudo', $newPseudo);

        $update->execute();

        $database = null;
    }

    /**
     * Fonction qui met à jour le mot de passe d'un utilisateur
     * @static
     * @param username nom d'utilisateur
     * @param newPasswd nouveau mot de passe de l'utilisateur
     */
    public static function updatePassword($username, $newPasswd) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'update/update_user_passwd.sql');

        $newPasswd = password_hash($newPasswd, PASSWORD_BCRYPT);

        $update = $pdo->prepare($script);
        $update->bindParam(':username', $username);
        $update->bindParam(':passwd', $newPasswd);

        $update->execute();

        $database = null;   
    }

    /**
     * Fonction qui supprime un utilisateur
     * @static
     * @param userId identifiant de l'utilisateur
     */
    public static function deleteUser($userId) {
        global $scriptPath, $configPath;
        $database = new Database($configPath . 'config.txt');
        $pdo = $database->getPdo();
        $script = file_get_contents($scriptPath . 'delete/delete_user.sql');

        $delete = $pdo->prepare($script);
        $delete->bindParam(':userid', $userId);

        $delete->execute();

        $database = null;   
    }
}
?>