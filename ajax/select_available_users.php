<?php

/**
 * Contrôleur ajax pour la sélection d'utilisateurs
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserModel.php';
require '../models/KanbanModel.php';

if ($isLogged) { // Si connecté
    if (isset($_GET['kanbanId'])) {
        // On récupère le kanban
        $kanban = KanbanModel::selectKanbanWithId($_GET['kanbanId']);
        if ($kanban[0]['ManagerId'] == $_SESSION['user']['UserId']) { // On vérifie si l'utilisateur est gestionnaire
            // On récupère les utilisateurs du kanban
            $users = UserModel::selectAvailableUsers($_GET['kanbanId']);
            $success = true;
        } else {
            $success = false;
        }
    } else {
        $success = false;
    }
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/AvailableUsersJson.php';
?>