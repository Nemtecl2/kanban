<?php

/**
 * Contrôleur ajax pour sélectionner tous les utilisateurs sauf celui courant
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserModel.php';

if ($isLogged) { // Si connecté
    // On récupère tous les utilisateurs sauf celui courant
    $users = UserModel::selectUsersExceptOne($_SESSION['user']['UserId']);
    $success = true;
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/UsersExceptOneJson.php';
?>