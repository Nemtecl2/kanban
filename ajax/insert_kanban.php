<?php

/**
 * Contrôleur ajax pour l'insertion d'un kanban
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/KanbanModel.php';
require '../models/UserKanbanModel.php';
require '../models/ListModel.php';

if ($isLogged) { // Si connecté
    // On sélectionne le kanban
    $kanban = KanbanModel::selectKanban($_POST['kanban']['name']);
    if (count($kanban)) { // On vérifie que le kanban n'existe pas déjà
        $success = false;
        $message = "Le nom " . $_POST['kanban']['name'] . " est déjà utilisé";
    } else {
        // On insère le kanban
        KanbanModel::insertKanban($_SESSION['user']['UserId'], $_POST['kanban']['name'], $_POST['kanban']['status']);
        // Si la liste des utilisateurs n'est pas vide
        if (!empty($_POST['kanban']['users'])) {
            // Pour chaque utilisateur, on l'ajoute en tant qu'invité au kanban
            foreach ($_POST['kanban']['users'] as $userId) {
                UserKanbanModel::insertGuest(intval($userId));
            }
        }

        // Si la liste des listes n'est pas vide
        if (!empty($_POST['kanban']['lists'])) {
            // Pour chaque liste, on l'ajoute dans le kanban
            foreach ($_POST['kanban']['lists'] as $listName) {
                ListModel::insertList($listName);
            }
        }
        $success = true;
        $message = "Le tableau a été créé";
    }

} else {
    $success = false;
    $message = "Veuillez vous connecter";
}

// Réponse à la requête au format JSON
require '../views/json/ResultJson.php';
?>