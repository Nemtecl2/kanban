<?php

/**
 * Contrôleur ajax pour mettre à jour une tâche
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';
require '../models/TaskModel.php';
require '../models/KanbanModel.php';

if ($isLogged) { // Si connecté
    // On récupère le rôle de l'utilisateur
    $kanban = UserKanbanModel::selectUserKanbanWithIds($_POST['kanbanId'], $_SESSION['user']['UserId']);
    // On vérifie que la chaîne à une taille supérieur à 0
    if (!empty($_POST['description'])) { 
        // On peut modifier seulement si on est invité ou guest
        $date = empty($_POST['date']) ? null : $_POST['date'];
        $userId = empty($_POST['userId']) ? null : $_POST['userId'];
        if ($kanban[0]['Role'] == 'manager') { // Si l'utilisateur est gestionnaire
            // On met à jour la tâche
            TaskModel::updateTask($userId, $_POST['taskId'], $_POST['description'], $date);
            // On sélectionne l'ensemble des informations d'un kanban qui seront utiles à la réponse de la requête
            $kanbanListTask = KanbanModel::selectKanbanListTask($_POST['kanbanId']);
            $success = true;
        } else if ($kanban[0]['Role'] == 'guest') { // Si l'utilisateur est invité
            // Si on est invité on ne peut pas affecter quelqu'un d'autre à une tâche
            if ($userId != null && $userId != $_SESSION['user']['UserId']) {
                $success = false;
            } else {
                // On met à jour la tâche
                TaskModel::updateTask($userId, $_POST['taskId'], $_POST['description'], $date);
                // On sélectionne l'ensemble des informations d'un kanban qui seront utiles à la réponse de la requête
                $kanbanListTask = KanbanModel::selectKanbanListTask($_POST['kanbanId']);
                $success = true;
            }
        } else { // Sinon on est lecteur, donc on ne peut pas modifier
            $success = false;
        }
    } else {
        $success = false;
    }
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/KanbanListTaskJson.php';
?>