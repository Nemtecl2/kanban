<?php

/**
 * Contrôleur ajax pour récupérer les informations d'un kanban
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/KanbanModel.php';

if (isset($_GET['kanbanId'])) {
    // On récupère les informations du kanban
    $kanbanListTask = KanbanModel::selectKanbanListTask($_GET['kanbanId']);
    $success = true;
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/KanbanListTaskJson.php';
?>