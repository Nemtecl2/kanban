<?php

/**
 * Contrôleur ajax pour sélectionner les tâches d'un utilisateur
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/TaskModel.php';

if ($isLogged) { // Si connecté
    // On récupère les tâches de l'utilisateur
    $tasks = TaskModel::selectTasks($_SESSION['user']['UserId']);
    $userId = $_SESSION['user']['UserId'];
    $success = true;
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/TasksJson.php';
?>