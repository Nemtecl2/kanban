<?php

/**
 * Contrôleur ajax pour la suppression d'une tâche depuis un kanban
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/TaskModel.php';

if ($isLogged) { // Si connecté
    // On sélectionne la tâche
    $task = TaskModel::selectTask($_SESSION['user']['UserId'], $_POST['id']);
    // On supprime la tâche
    TaskModel::deleteTask($_POST['id']);
    $success = true;
    $message = "La tâche a été supprimée";
} else {
    $success = false;
    $message = "Veuillez vous connecter";
}

// Réponse à la requête au format JSON
require '../views/json/ResultJson.php';
?>