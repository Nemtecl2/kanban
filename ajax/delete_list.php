<?php

/**
 * Contrôleur ajax pour la suppression d'une liste
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';
require '../models/ListModel.php';
require '../models/KanbanModel.php';

if ($isLogged) { // Si connecté
    // On sélectionne le rôle de l'utilisateur dans le kanban
    $kanban = UserKanbanModel::selectUserKanbanWithIds($_POST['kanbanId'], $_SESSION['user']['UserId']);
    if ($kanban[0]['Role'] == 'manager') { // Si l'utilisateur est gestionnaire du kanban
        // On supprime la liste
        ListModel::deleteList($_POST['listId']);
        // On sélectionne l'ensemble des informations d'un kanban qui seront utiles à la réponse de la requête
        $kanbanListTask = KanbanModel::selectKanbanListTask($_POST['kanbanId']);
        $success = true;
    } else {
        $success = false;
    }
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/KanbanListTaskJson.php';
?>