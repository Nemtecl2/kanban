<?php

/**
 * Contrôleur ajax pour mettre à jour une liste
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';
require '../models/ListModel.php';
require '../models/KanbanModel.php';

if ($isLogged) { // Si connecté
    // On récupère le rôle de l'utilisateur au sein du kanban
    $kanban = UserKanbanModel::selectUserKanbanWithIds($_POST['kanbanId'], $_SESSION['user']['UserId']);
    if ($kanban[0]['Role'] == 'manager') { // Si l'utilisateur est gestionnaire
        // On récupère toutes listes sauf celle courante
        $lists = ListModel::selectListsExceptOne($_POST['kanbanId'], $_POST['list']['id']);
        $lists = array_map('strtolower', array_column($lists, 'Name'));
        // Si le nom de la liste n'est pas déjà utilisé, on met à jour la liste
        if (!in_array(strtolower($_POST['list']['name']), $lists)) {
            ListModel::updateList($_POST['list']['id'], $_POST['list']['name']);
            // On sélectionne l'ensemble des informations d'un kanban qui seront utiles à la réponse de la requête
            $kanbanListTask = KanbanModel::selectKanbanListTask($_POST['kanbanId']);
            $success = true;
        } else {
            $success = false;
        }
    } else {
        $success = false;
    }
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/KanbanListTaskJson.php';
?>