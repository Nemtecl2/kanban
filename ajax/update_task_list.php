<?php

/**
 * Contrôleur ajax pour mettre à jour la liste auquel appartient la tâche
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';
require '../models/TaskModel.php';
require '../models/KanbanModel.php';

if ($isLogged) { // Si connecté
    // On récupère le rôle de l'utilisateur au sein du kanban
    $kanban = UserKanbanModel::selectUserKanbanWithIds($_POST['kanbanId'], $_SESSION['user']['UserId']);
    if ($kanban[0]['Role'] == 'manager' || $kanban[0]['Role'] == 'guest') { // S'il est gestionnaire ou invité
        // On met à jour la liste auquel appartient la tâche
        TaskModel::updateTaskList($_POST['taskId'], $_POST['listId']);
        $success = true;
        $message = 'La carte a bien été déplacée';
    } else {
        $success = false;
        $message = 'Vous ne pouvez pas déplacer une carte en tant que lecteur';
    }
} else {
    $success = false;
    $message = 'Vous ne pouvez pas déplacer une carte en tant que lecteur';
}

// Réponse à la requête au format JSON
require '../views/json/ResultJson.php';
?>