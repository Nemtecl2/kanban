<?php

/**
 * Contrôleur ajax pour sélectionner les droits de l'utilisateur
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';

if ($isLogged) { // Si connecté
    if (isset($_GET['kanbanId'])) {
        // On récupère le rôle de l'utilisateur au sein du kanban
        $role = UserKanbanModel::selectRole($_SESSION['user']['UserId'], $_GET['kanbanId']);
        if (count($role)) { // Gestionnaire ou invité
            $rights = $role[0]['Role'];
        } else { // Lecteur
            $rights = 'reader';
        }
        $success = true;
    } else { // Cas d'erreur dans les paramètres de la requête
        $success = false;
    }
} else { // Droit en tant qu'anonyme
    $success = true;
    $rights = 'reader';
}

// Réponse à la requête au format JSON
require '../views/json/UserRightsJson.php';
?>