<?php
/**
 * Contrôleur ajax pour la sélection d'utilisateur par leur rôle
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserModel.php';
require '../models/UserKanbanModel.php';

if ($isLogged) { // Si connecté
    if (isset($_GET['kanbanId'])) { // Vérification des paramètres de la requête
        // On récupère le rôle de l'utilisateur au sein du kanban
        $role = UserKanbanModel::selectRole($_SESSION['user']['UserId'], $_GET['kanbanId']);
        if (count($role)) { // Gestionnaire ou invité
            if ($role[0]['Role'] === 'manager') { // Gestionnaire
                // Faire un select des invité
                $users = UserModel::selectAvailableUsersByRole($_GET['kanbanId']);

            } else if ($role[0]['Role'] === 'guest') { // Invité
                $users = array(array(
                    'userId' => $_SESSION['user']['UserId'],
                    'pseudo' => $_SESSION['user']['Pseudo']
                ));
            }
            $success = true;
        } else {
            $success = false;
        }
    } else { // Paramètre manquant
        $success = false;
    }
} else { // Si déconnecté
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/AvailableUsersJson.php';
?>