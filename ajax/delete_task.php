<?php

/**
 * Contrôleur ajax pour la suppression d'une tâche
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';
require '../models/TaskModel.php';
require '../models/KanbanModel.php';

if ($isLogged) { // Si connecté
    // On sélectionne le rôle de l'utilisateur dans le kanban
    $kanban = UserKanbanModel::selectUserKanbanWithIds($_POST['kanbanId'], $_SESSION['user']['UserId']);
    if ($kanban[0]['Role'] == 'manager') { // Si l'utilisateur est gestionnaire du kanban
        // On supprime la tâche
        TaskModel::deleteTask($_POST['taskId']);
        // On sélectionne l'ensemble des informations d'un kanban qui seront utiles à la réponse de la requête
        $kanbanListTask = KanbanModel::selectKanbanListTask($_POST['kanbanId']);
        $success = true;
    } else if ($kanban[0]['Role'] == 'guest') { // Si l'utilisateur est invité au kanban
        // Si on est invité on ne peut pas affecter quelqu'un d'autre à une tâche
        if ($userId != null && $userId != $_SESSION['user']['UserId']) {
            $success = false;
        } else {
            // On supprime la tâche
            TaskModel::deleteTask($_POST['taskId']);
            // On sélectionne l'ensemble des informations d'un kanban qui seront utiles à la réponse de la requête
            $kanbanListTask = KanbanModel::selectKanbanListTask($_POST['kanbanId']);
            $success = true;
        }
    } else { // Sinon on est lecteur, donc on ne peut pas ajouté
        $success = false;
    }
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/KanbanListTaskJson.php';
?>