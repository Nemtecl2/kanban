<?php

/**
 * Contrôleur ajax pour l'insertion de listes
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';
require '../models/ListModel.php';
require '../models/KanbanModel.php';

if ($isLogged) { // Si connecté
    // On récupère le rôle de l'utilisateur au sein du kanban
    $kanban = UserKanbanModel::selectUserKanbanWithIds($_POST['kanbanId'], $_SESSION['user']['UserId']);
    // Si l'utilisateur est gestionnaire
    if ($kanban[0]['Role'] == 'manager') {
        // On sélectionne les listes courantes du kanban
        $currentLists = ListModel::selectLists($_POST['kanbanId']);
        $currentListsNames = array_map('strtolower', array_column($currentLists, 'Name'));
        // Pour chaque liste, on ajoute celles qui n'existent pas déjà
        foreach ($_POST['lists'] as $list) {
            if (!in_array(strtolower($list), $currentListsNames)) {
                ListModel::insertListInKanban($_POST['kanbanId'], $list);
            }
        }
        // On sélectionne l'ensemble des informations d'un kanban qui seront utiles à la réponse de la requête
        $kanbanListTask = KanbanModel::selectKanbanListTask($_POST['kanbanId']);
        $success = true;
    } else {
        $success = false;
    }
} else {
    $success = false;
}

// Réponse à la requête au format JSON
require '../views/json/KanbanListTaskJson.php';
?>