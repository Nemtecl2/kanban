<?php

/**
 * Contrôleur ajax pour la suppression d'un kanban
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/KanbanModel.php';

if ($isLogged) { // Si connecté
  // On sélectionne le kanban
  $kanban = KanbanModel::selectKanban($_POST['name']);

  if ($_SESSION['user']['UserId'] === $kanban[0]['ManagerId']) { // Si l'utilisateur est gestionnaire du kanban
    // On supprime le kanban
    KanbanModel::deleteKanban($_POST['name']);
    $success = true;
    $message = "Le tableau a été supprimé";
  } else {
    $success = false;
    $message = "Vous n'êtes pas propriétaire de ce tableau";
  }
} else {
  $success = false;
  $message = "Veuillez vous connecter";
}

// Réponse à la requête au format JSON
require '../views/json/ResultJson.php';
?>