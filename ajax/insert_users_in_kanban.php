<?php

/**
 * Contrôleur ajax pour l'insertion d'utilisateur dans un kanban
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';

if ($isLogged) { // Si connecté
    // On récupère le rôle de l'utilisateur au sein du kanban
    $kanban = UserKanbanModel::selectUserKanbanWithIds($_POST['kanbanId'], $_SESSION['user']['UserId']);
    if ($kanban[0]['Role'] == 'manager') { // Si l'utilisateur est gestionnaire
        // On récupère les utilisateurs du kanban
        $currentUsers = UserKanbanModel::selectUsersFromKanban($_POST['kanbanId']);
        $currentUsersIds = array_column($currentUsers, 'UserId');
        // Pour chaque utilisateur n'étant pas encore dans le kanban, on les ajoute
        foreach ($_POST['users'] as $user) {
            if (!in_array($user, $currentUsersIds)) {
                UserKanbanModel::insertGuestInKanban($_POST['kanbanId'], $user);
            }
        }
        $success = true;
        $message = 'Les utilisateurs ont bien été ajoutés au tableau';
    } else {
        $success = false;
        $message = "Vous n'êtes pas gestionnaire de ce tableau";
    }
} else {
    $success = false;
    $message = "Vous n'êtes pas connecté";
}

// Réponse à la requête au format JSON
require '../views/json/ResultJson.php';

?>