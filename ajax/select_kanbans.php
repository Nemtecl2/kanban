<?php

/**
 * Contrôleur ajax pour sélectionner les kanbans que l'utilisateur a accès
 */
session_start();
$modelPath = '../models/';
$scriptPath = '../scripts/';
$configPath = '../../';
$isLogged = isset($_SESSION['user']);
require '../models/UserKanbanModel.php';

if ($isLogged) { // Si connecté
    // On récupère les kanbans auquel l'utilisateur a accès
    $kanbans = UserKanbanModel::selectUserKanban($_SESSION['user']['UserId']);
} else {
    // On récupère les kanbans auquel l'utilisateur a accès
    $kanbans = UserKanbanModel::selectKanbanPublic();
}

// Réponse à la requête au format JSON
require '../views/json/KanbansJson.php';
?>