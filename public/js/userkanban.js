$(document).ready(function () {
    initializeKanbans($('.select-kanbans').val());
});

/**
 * Fonction qui récupère tous les kanbans en fonction du filtre sélectionné puis appelle la vue pour l'affichage
 * @param filter filtre sélectionné
 */
function initializeKanbans(filter) {
    $.ajax({
        url: './ajax/select_kanbans.php',
        type: 'GET',
        dataType: 'json',
        success: (data) => {
            var kanbans = UserKanbanModel.initKanbanList(data, filter);
            var isLogged = data.isLogged;
            displayKanbans(isLogged, kanbans, filter);

            $('.select-kanbans').change(function(evt) {
                initializeKanbans(evt.target.value);
            });
        }
    });
}

/**
 * Fonction qui rend la suppression possible
 */
function makeDeletionPossible() {
    $('#kanbans tbody').on('click', '.delete-kanban', function () {
        // Récupère le nom du kanban à supprimer
        var kanbanName = $($('.delete-kanban').parent().siblings('.kanban-name')[0]).text();
        var confirmMsg = 'Êtes-vous sûr de vouloir supprimer le kanban "' + kanbanName + '" ?';

        modalConfirm(kanbanName, confirmMsg);
    });

    $('#kanbans tbody').on('click', '.deletion-disabled', function () {
        toastr.error("Vous n'êtes pas propriétaire de ce tableau", "Erreur");
    });
}

/**
 * Fonction qui supprime un kanban
 * @param kanbanName nom du kanban à supprimer 
 */
function deleteKanban(kanbanName) {
    $.ajax({
        url: './ajax/delete_kanban.php',
        type: 'POST',
        dataType: 'json',
        data: { name: kanbanName },
        success: (data) => {
            if (data.success) {
                initializeKanbans($('.select-kanbans').val());
                this.toastr.success(data.message, 'Bravo');
            } else {
                this.toastr.error(data.message, 'Erreur');
            }
        }
    });
}