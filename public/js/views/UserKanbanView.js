/**
 * Fonction qui affiche le kanban
 * @param isLogged booléen valant true si l'utilisateur est connecté, faux sinon
 * @param kanbans tableau de l'ensemble des kanbans
 * @param filter filtre appliqué au tableau
 */
function displayKanbans(isLogged, kanbans, filter) {
    $('#kanbans').DataTable().destroy();
    $("#kanbans tbody").empty();

    // On créé une ligne du tableau par kanban
    for (var i = 0; i < kanbans.length; ++i) {
        var elements = "<tr>";
        elements += "<td class='w-40 kanban-name'>" + kanbans[i].name + "</td>";
        if (kanbans[i].role) {
            elements += "<td class='w-15'>" + kanbans[i].role + "</td>";
        }
        elements += "<td class='w-15'>" + kanbans[i].manager + "</td>";
        elements += "<td class='w-15 text-center'><span class='d-none'>" 
        + ((kanbans[i].status === 'public') ? "Public" : "Privé") + "</span><a data-toggle='tooltip' data-placement='top' title='" 
        + ((kanbans[i].status === 'public') ? "Public" : "Privé") + "'><i class='fa fa-eye" 
        + ((kanbans[i].status === 'public') ? "" : "-slash") + "' /></a></td>"
        elements += "<td class='text-right w-15'><a class='view-kanban' href='kanban.php?kanbanId=" + kanbans[i].kanbanId + "' data-toggle='tooltip' data-placement='top' title='Consulter le kanban'><i class='fab fa-trello'/></a>" 

        if (isLogged) {
            elements += "<a class='ml-2 " + ((kanbans[i].role === 'Gestionnaire') ? "delete-kanban" : "deletion-disabled")
                +"' data-toggle='tooltip' data-placement='top' title='Supprimer'><i class='fa fa-times'/></a>";
        }
        elements += "</td></tr>";

        $("#kanbans tbody").append(elements);
        $('[data-toggle="tooltip"]').tooltip();
    }

    // On créé la table
    var table = $('#kanbans').DataTable({
        pageLength: 5,
        bLengthChange: false,
        language: {
            sProcessing: "Traitement en cours ...",
            sLengthMenu: "Afficher _MENU_ lignes",
            sZeroRecords: "Aucun résultat trouvé",
            sEmptyTable: "Aucune donnée disponible",
            sInfo: "Lignes _START_ à _END_ sur _TOTAL_",
            sInfoEmpty: "Aucune ligne affichée",
            sInfoFiltered: "(Filtrer un maximum de _MAX_)",
            sInfoPostFix: "",
            sSearch: "Chercher:",
            sUrl: "",
            sInfoThousands: ",",
            sLoadingRecords: "Chargement...",
            oPaginate: {
                sFirst: "Premier",
                sLast: "Dernier",
                sNext: "Suivant",
                sPrevious: "Précédent"
            },
            oAria: {
                sSortAscending: ": Trier par ordre croissant",
                sSortDescending: ": Trier par ordre décroissant"
            }
        }
    });

    // On ajoute un icône de tri sur chaque colonne
    table.columns().iterator('column', function (ctx, idx) {
        $(table.column(idx).header()).append('<span class="sort-icon"/>');
    });
    makeDeletionPossible();

    if (isLogged) {
        var select = `<select class="custom-select select-kanbans">
                            <option value="all" ${filter === 'all' ? 'selected' : ''}> Afficher tous les Kanbans</option>
                            <option value="manager" ${filter === 'manager' ? 'selected' : ''}>Afficher les Kanbans gérés</option>
                            <option value="guest" ${filter === 'guest' ? 'selected' : ''}>Afficher les Kanbans auxquels je participe</option>
                        </select>`;
        $("#kanbans_wrapper .row:first div:first").append(select);
    }
}

/**
 * Fenêtre de confirmation
 * @param kanbanName nom du kanban
 * @param msgValidation le message apparaîssant dans la fenêtre de confirmation
 */
function modalConfirm(kanbanName, confirmMsg) {
    $('#modal-confirm').modal({ backdrop: 'static', keyboard: false })
    $('#modal-confirm').modal('show');
    $('#modal-confirm .modal-body').html(confirmMsg);

    $('#modal-confirm .close, #modal-confirm #no-confirm-dialog').off('click').on('click', function () {
        $('#modal-confirm').modal('hide');
        // Ne rien faire
    });

    $('#modal-confirm #yes-confirm-dialog').off('click').on('click', function () {
        $('#modal-confirm').modal('hide');
        deleteKanban(kanbanName);
    });
}