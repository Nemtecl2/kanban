$(document).ready(function() {
    $("#lists").select2({
        theme: 'bootstrap4',
        placeholder: 'Créer des listes',
        allowClear: true,
        tags: true,
        tokenSeparators: [',']
    });
});

/**
 * Fonction qui ajoute les utilisateurs à la sélection
 * @param users les utilisateurs à ajouter
 */
function addUserToSelect(users) {
    users.forEach((u) => {
        u.text = u.pseudo;
        delete u.pseudo;
    });

    $("#user-list").select2({
        theme: 'bootstrap4',
        placeholder: 'Inviter des utilisateurs',
        allowClear: true,
        tokenSeparators: [',', ' '],
        data: users
    });
}