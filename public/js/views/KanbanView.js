$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    var mediumEditor = new MediumEditor('.medium-editor', {
        toolbar: {
            buttons: ['bold', 'italic', 'underline', 'strikethrough'],
        },
        placeholder: {
            text: ''
        }
    });
});

/**
 * Fonction qui efface le contenu du kanban
 */
function clearKanbanContainer() {
    $('#kanban-container').empty();
}

/**
 * Fonction qui créé la possibilité d'ajouter des listes
 */
function addListsToSelect() {
    $("#add-lists").empty().trigger('change');
    $("#add-lists").select2({
        theme: 'bootstrap4',
        placeholder: 'Créer des listes',
        allowClear: true,
        tags: true,
        tokenSeparators: [',']
    });
}

/**
 * Fonction qui ajoute les utilisateurs à la sélection pour l'ajout de membres
 * @param users ensemble des utilisateurs
 */
function addUsersToSelect(users) {
    $("#add-users").empty().trigger('change');
    users.forEach((u) => {
        u.id = u.userId;
        delete u.userId;
        u.text = u.pseudo;
        delete u.pseudo;
    });

    $('#add-users').select2({
        theme: 'bootstrap4',
        placeholder: 'Inviter des utilisateurs',
        allowClear: true,
        tokenSeparators: [',', ' '],
        data: users
    });
}

/**
 * Fonction qui ajoute les utilisateurs à la sélection pour l'ajout d'un membre à une tâche
 * @param users ensemble des utilisateurs
 */
function addTaskUsersToSelect(users) {
    $('#add-task-date-timepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#add-task-date-timepicker').on('change.datetimepicker', function (e) {
        if (!$('#add-task-date').val().match('^[0-9]')) {
            $('#add-task-date').val('');
        }
    });

    $("#add-task-user").empty().trigger('change');
    users.forEach((u) => {
        u.id = u.userId;
        delete u.userId;
        u.text = u.pseudo;
        delete u.pseudo;
    });

    $("#add-task-user").select2({
        theme: 'bootstrap4',
        placeholder: 'Ajouter un utilisateur',
        allowClear: true,
        data: users,
        minimumResultsForSearch: -1
    });
    $('#add-task-user').val(null).trigger('change');


    $('#change-task-date-timepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#change-task-date-timepicker').on('change.datetimepicker', function (e) {
        if (!$('#change-task-date').val().match('^[0-9]')) {
            $('#change-task-date').val('');
        }
    });

    $("#change-task-user").empty().trigger('change');
    $("#change-task-user").select2({
        theme: 'bootstrap4',
        placeholder: 'Ajouter un utilisateur',
        allowClear: true,
        minimumResultsForSearch: -1,
        data: users
    });
    $('#change-task-user').val(null).trigger('change');
}

/**
 * Fonction qui créé le kanban
 * @param kanbanListTask toutes les informations nécessaire à un kanban (kanban, listes, tâches)
 * @param userRights droits de l'utilisateur courant
 */
function displayKanban(kanbanListTask, userRights) {
    $('#kanban-container').empty();
    var userId = $('#user-id').val();

    var elements = '';
    var lists = kanbanListTask.lists;
    lists.forEach((list) => {
        elements += "<div id='list-" + list.id + "' class='kanban-list'>";
        elements += "<span class='list-header'>" + list.name + "</span>";
        if (userRights === 'manager') {
            elements += "<a id='edit-list-" + list.id + "' class='text-white' href='#change-list-modal' data-toggle='modal'><i class='fas fa-edit float-right'></i></a>";
        }
        elements += "<div class='task-container'>";

        var tasks = list.tasks;
        tasks.forEach((task) => {
            var canUseTask = ((userRights === 'manager' || userId == task.userId) || (userRights === 'guest' && task.userId === null));
            elements += "<div id='task-" + task.taskId + "' class='kanban-task" + ((canUseTask) ? ' task-draggable' : '') + "'>";
            elements += "<input type='hidden' id='task-user-id" + ((task.userId === null) ? '' : "-" + task.userId) 
                + "' value='" + ((task.userId === null) ? '' : "" + task.userId) + "' />";
            elements += "<div class='row w-100 ml-0'>";
            elements += "<div class='task-description col-md-10 pl-1 pr-1'>" + task.description + "</div>";
            if (canUseTask) {
                elements += "<div class='col-md-2 pr-1 text-right'><i class='fa fa-edit'></i></div>"
            }
            elements += "</div>";
            elements += "<div class='task-date'>" + (task.date ? task.date : "<i class='fas fa-calendar-times'></i>") + "</div>";
            elements += "<div class='task-attribution'>" + (task.username ? task.username : "<i class='fas fa-user-slash'></i>") + "</div>";
            // Fin de la tâche
            elements += "</div>";
        });

        // Fin des tâches
        elements += "</div>";
        
        if (userRights === 'manager' || userRights === 'guest') {
            elements += "<div class='add-task'><a id='add-task-to-list-" + list.id + "' class='text-white' href='#add-task-modal' data-toggle='modal'>" +
                "<i class='far fa-plus-square mr-1'></i>Ajouter une carte...</a></div>";
        }
        // Fin de la liste
        elements += "</div>";
    });

    $("#kanban-container").append(elements);
    $('[data-toggle="tooltip"]').tooltip();

    // Si l'utilisateur est gestionnaire
    if (userRights === 'manager') {
        lists.forEach((list) => {
            $('#edit-list-' + list.id).on('click', function () {
                $('#current-list-id').val(list.id);
                $('#update-list-name').val(list.name);
            });
        });
    }
    
    lists.forEach((list) => {
        var tasks = list.tasks;
        tasks.forEach((task) => {
            if ((task.userId == null && userRights === 'guest') || task.userId == userId || userRights === 'manager') {
                $('#task-' + task.taskId).on('click', function() {
                    initializeAvailableUsersByRole();
                    $('#change-task-modal').modal('show');
                    $('#change-task-id').val($(this).attr('id').match(/\d+/)[0]);
                    $('#change-task-description').html(task.description).trigger('change');
                    $('#change-task-date').val(task.date).trigger('change');
                    $('#change-task-user').val(task.userId).trigger('change');
                });
            }
        });
    });

    // Pour ajouter une tâche
    $('.add-task a').on('click', function() {
        initializeAvailableUsersByRole();
        $('#add-task-list-id').val($(this).attr('id').match(/\d+/)[0]);
    });

    initializeDrag(userRights);
}