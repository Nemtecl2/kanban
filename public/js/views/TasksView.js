/**
 * Fonction qui affiche les tâches pour un utilisateur donné
 * @param tasks tableau de l'ensemble des tâches 
 * @param userId utilisateur courant
 */
function displayTasks(tasks, userId) {
    $('#tasks').DataTable().destroy();
    $("#tasks tbody").empty();

    // On créé une ligne du tableau par tâche
    for (var i = 0; i < tasks.length; ++i) {
        var elements = "<tr>";
        elements += "<td class='kanban-name'>" + tasks[i].kanbanName + "</td>";
        elements += "<td class='list-name'>" + tasks[i].listName + "</td>";
        elements += "<td class='task-description'><input name='task-id' type='hidden' value='" 
            + tasks[i].taskId + "'>" + tasks[i].description + "</td>";
        elements += "<td>" + ((tasks[i].date === null ) ? "Aucune date limite" : tasks[i].date)
            + "</td>";
        elements += "<td class='text-right'><a class='view-kanban' href='kanban.php?kanbanId=" + tasks[i].kanbanId +  "' data-toggle='tooltip' data-placement='top' title='Consulter le kanban'><i class='fab fa-trello'/>"
                + "<a class='ml-2 delete-task' data-toggle='tooltip' data-placement='top' title='Supprimer'><i class='fa fa-times'/></a>";
        elements += "</td>";
        elements += "</tr>";

        $("#tasks tbody").append(elements);
        
    }

    // On créé la table
    var table = $('#tasks').DataTable({
        pageLength: 5,
        bLengthChange: false,
        language: {
            sProcessing: "Traitement en cours ...",
            sLengthMenu: "Afficher _MENU_ lignes",
            sZeroRecords: "Aucun résultat trouvé",
            sEmptyTable: "Aucune donnée disponible",
            sInfo: "Lignes _START_ à _END_ sur _TOTAL_",
            sInfoEmpty: "Aucune ligne affichée",
            sInfoFiltered: "(Filtrer un maximum de _MAX_)",
            sInfoPostFix: "",
            sSearch: "Chercher:",
            sUrl: "",
            sInfoThousands: ",",
            sLoadingRecords: "Chargement...",
            oPaginate: {
                sFirst: "Premier",
                sLast: "Dernier",
                sNext: "Suivant",
                sPrevious: "Précédent"
            },
            oAria: {
                sSortAscending: ": Trier par ordre croissant",
                sSortDescending: ": Trier par ordre décroissant"
            }
        }
    });

    // On ajoute un icône de tri sur chaque colonne
    table.columns().iterator('column', function (ctx, idx) {
        $(table.column(idx).header()).append('<span class="sort-icon"/>');
    });

    makeDeletionPossible();
}

/**
 * Fenêtre de confirmation
 * @param taskId identifiant de la tâche
 * @param msgValidation le message apparaîssant dans la fenêtre de confirmation
 */
function modalConfirm(taskId, confirmMsg) {
    $('#modal-confirm').modal({ backdrop: 'static', keyboard: false })
    $('#modal-confirm').modal('show');
    $('#modal-confirm .modal-body').html(confirmMsg);

    $('#modal-confirm .close, #modal-confirm #no-confirm-dialog').off('click').on('click', function () {
        $('#modal-confirm').modal('hide');
        // Ne rien faire
    });

    $('#modal-confirm #yes-confirm-dialog').off('click').on('click', function () {
        $('#modal-confirm').modal('hide');
        deleteTask(taskId);
    });
}
