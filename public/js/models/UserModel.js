/**
 * @class UserModel
 */
class UserModel {

    /**
     * Constructeur de la classe
     * @constructor
     * @param id identifiant de l'utilisateur
     * @param pseudo pseudo de l'utilisateur
     */
    constructor(id, pseudo) {
        this.id = id;
        this.pseudo = pseudo;
    }

    /**
     * Fonction qui attribue l'identifiant de l'utilisateur
     * @param id identifiant de l'utilisateur
     */
    setId(id) {
        this.id = id;
    }

    /**
     * Fonction qui attribue le pseudo de l'utilisateur
     * @param pseudo  pseudo de l'utilisateur
     */
    setPseudo(pseudo) {
        this.pseudo = pseudo;
    }

    /**
     * Fonction qui initialise la liste des utilisateurs
     * @static
     * @param data objet généré par la réponse de la requête
     * @returns users l'ensemble des utilisateurs (sous forme d'un tableau de UserModel)
     */
    static initUserList(data) {
        var users = [];
        for (var i = 0; i < data.users.length; ++i) {
            var currentUser = data.users[i];
            users.push(new UserModel(currentUser.id, currentUser.pseudo));
        }
        return users;
    }
}