/**
 * @class TaskModel
 */
class TaskModel {

    /**
     * Constructeur de la classe
     * @constructor
     * @param taskId identifiant de la tâche
     * @param kanbanId identifiant du kanban auquel appartient la tâche
     * @param kanbanName nom du kanban auquel appartient la tâche
     * @param listName nom de la liste auquel appartient la tâche
     * @param description description de la tâche
     * @param date date limite de la tâche
     * @param kanbanManagerId identifiant du gestionnaire du kanban auquel appartient la tâche
     * @param userId identifiant de l'utilisateur associé à la tâche
     * @param username nom d'utilisateur associé à la tâche
     * @param listId identifiant de la liste auquel appartient la tâche
     */
    constructor(taskId, kanbanId, kanbanName, listName, description, date, kanbanManagerId, userId, username, listId) {
        this.taskId = taskId;
        this.description = description;
        this.date = date;

        if (kanbanId !== undefined) {
            this.kanbanId = kanbanId;
        }

        if (kanbanName !== undefined) {
            this.kanbanName = kanbanName;
        }

        if (listName !== undefined) {
            this.listName = listName;
        }

        if (kanbanManagerId !== undefined) {
            this.kanbanManagerId = kanbanManagerId;
        }

        if (userId !== undefined) {
            this.userId = userId;
        }

        if (username !== undefined) {
            this.username = username;
        }

        if (listId !== undefined) {
            this.listId = listId;
        }
    }

    /**
     * Fonction qui attribue un identifiant à la tâche
     * @param taskId identifiant de la tâche
     */
    setTaskId(taskId) {
        this.taskId = taskId;
    }

    /**
     * Fonction qui attribue l'identifiant du kanban auquel appartient la tâche
     * @param kanbanId identfiant du kanban auquel appartient la tâche
     */
    setKanbanId(kanbanId) {
        this.kanbanId = kanbanId;
    }

    /**
     * Fonction qui attribue le nom du kanban auquel appartient la tâche
     * @param kanbanName nom du kanban auquel appartient la tâche
     */
    setKanbanName(kanbanName) {
        this.kanbanName = kanbanName;
    }

    /**
     * Fonction qui attribue le nom de la liste auquel appartient la tâche
     * @param listName nom de la liste auquel appartient la tâche
     */
    setListName(listName) {
        this.listName = listName;
    }

    /**
     * Fonction qui attribue la description de la tâche
     * @param description description de la tâche
     */
    setDescription(description) {
        this.description = description;
    }

    /**
     * Fonction qui attribue la date limite de la tâche
     * @param date date limite de la tâche
     */
    setDate(date) {
        this.date = date;
    }

    /**
     * Fonction qui attribue l'identifiant du gestionnaire du kanban auquel appartient la tâche
     * @param kanbanManagerId identifiant du gestionnaire du kanban auquel appartient la tâche
     */
    setKanbanManagerId(kanbanManagerId) {
        this.kanbanManagerId = kanbanManagerId;
    }

    /**
     * Fonction qui attribue l'identifiant de l'utilisateur associé à la tâche
     * @param userId identifiant de l'utilisateur associé à la tâche
     */
    setUserId(userId) {
        this.userId = userId;
    }

    /**
     * Fonction qui attribue le nom d'utilisateur associé à la tâche
     * @param username nom d'utilisateur associé à la tâche
     */
    setUsername(username) {
        this.username = username;
    }

    /**
     * Fonction qui attribue l'identifiant de la liste auquel appartient la tâche
     * @param listId identifiant de la liste auquel appartient la tâche
     */
    setListId(listId) {
        this.listId = listId;
    }

    /**
     * Fonction qui initialise l'ensemble des tâches
     * @static
     * @param data objet généré par la réponse de la requête
     * @returns tasks l'ensemble des tâches (sous forme d'un tableau de TaskModel)
     */
    static initTaskList(data) {
        var tasks = [];
        for (var i = 0; i < data.tasks.length; ++i) {
            var currentTask = data.tasks[i];
            tasks.push(new TaskModel(currentTask.taskId, currentTask.kanbanId, currentTask.kanbanName, currentTask.listName, 
                currentTask.description, currentTask.date, currentTask.kanbanManagerId));
        }
        return tasks;
    }
}