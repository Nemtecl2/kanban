/**
 * @class KanbanModel
 */
class KanbanModel {

    /**
     * Constructeur de classe
     * @constructor
     * @param name nom du kanban
     * @param status statut du kanban (public ou privé)
     * @param users utilisateurs du kanban
     * @param lists listes du kanban
     * @param id identifiant du kanban
     * @param managerId identifiant du gestionnaire du kanban
     * @param managerName nom du gestionnaire du kanban
     * @param managerPseudo pseudo du gestionnaire du kanban
     */
    constructor(name, status, users, lists, id, managerId, managerName, managerPseudo) {
        this.name = name;
        this.status = status;
        this.users = users;
        this.lists = lists;

        if (users !== undefined) {
            this.users = users;
        }

        if (lists !== undefined) {
            this.lists = lists;
        }

        if (id !== undefined) {
            this.id = id;
        }

        if (managerId !== undefined) {
            this.managerId = managerId;
        }

        if (managerName !== undefined) {
            this.managerName = managerName;
        }

        if (managerPseudo !== undefined) {
            this.managerPseudo = managerPseudo;
        }
    }

    /**
     * Fonction qui attribue le nom du kanban
     * @param name nom du kanban
     */
    setName(name) {
        this.name = name;
    }

    /**
     * Fonction qui attribue le statut du kanban
     * @param status statut du kanban
     */
    setStatus(status) {
        this.status = status;
    }

    /**
     * Fonction qui attribue l'ensemble des utilisateurs du kanban
     * @param users liste d'utilisateurs
     */
    setUsers(users) {
        this.users = users;
    }

    /**
     * Fonction qui attribue l'ensemble des listes du kanban
     * @param lists liste de listes
     */
    setLists(lists) {
        this.lists = lists;
    }

    /**
     * Fonction qui attribue un identifiant au kanban
     * @param id identifiant du kanban
     */
    setId(id) {
        this.id = id;
    }

    /**
     * Fonction qui attribue l'identifiant au gestionnaire du kanban
     * @param managerId identifiant du gestionnaire du kanban
     */
    setManagerId(managerId) {
        this.managerId = managerId;
    }

    /**
     * Fonction qui attribue le nom du gestionnaire du kanban
     * @param managerName nom du gestionnaire du kanban
     */
    setManagerName(managerName) {
        this.managerName = managerName;
    }

    /**
     * Fonction qui attribue le pseudo du gestionnaire du kanban
     * @param managerPseudo pseudo du gestionnaire du kanban
     */
    setManagerPseudo(managerPseudo) {
        this.managerPseudo = managerPseudo;
    }

    /**
     * Fonction qui initialise le kanban
     * @static
     * @param data objet généré par la réponse de la requête
     * @returns un KanbanModel avec les informations reçues
     */
    static initKanban(data) {
        var kanbanName = data[0].kanbanName;
        var kanbanId = data[0].kanbanId;
        var managerId = data[0].managerId;
        var distinctLists = data.filter((l, pos, arr) => {
            return arr.map(mapL => mapL.listId).indexOf(l.listId) === pos;
        });
        var tasks = data.map(t => {
            return new TaskModel(t.taskId, kanbanId, kanbanName, t.listName, t.description, 
                t.date, managerId, t.userId, t.pseudo, t.listId);
        });
        var lists = distinctLists.map(l => {
            return new ListModel(l.listId, l.listName, tasks.filter(t => t.listId === l.listId 
                && t.description !== null));
        });
        
        return new KanbanModel(kanbanName, data[0].status, null, lists, kanbanId, managerId, 
            data[0].managerName, data[0].managerPseudo);
    }
}