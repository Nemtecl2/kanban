/**
 * @class UserKanbanModel
 */
class UserKanbanModel {

    /**
     * Constructeur de la classe
     * @constructor
     * @param kanbanId identifiant du kanban
     * @param name nom du kanban
     * @param status statut du kanban
     * @param manager gestionnaire du kanban
     * @param role role de l'utilisateur dans le kanban
     */
    constructor(kanbanId, name, status, manager, role) {
        this.kanbanId = kanbanId;
        this.name = name;
        this.status = status;
        this.manager = manager;

        if (role !== undefined) {
            this.role = role;
        }
    }

    /**
     * Fonction qui attribue l'identifiant du kanban
     * @param kanbanId identifiant du kanban
     */
    setKanbanId(kanbanId) {
        this.kanbanId = kanbanId;
    }

    /**
     * Fonction qui attribue le nom du kanban
     * @param name nom du kanban
     */
    setName(name) {
        this.name = name;
    }

    /**
     * Fonction qui attribue le statut du kanban
     * @param status statut du kanban (public ou privé)
     */
    setStatus(status) {
        this.status = status;
    }

    /**
     * Fonction qui attribue le gestionnaire du kanban
     * @param manager gestionnaire du kanban
     */
    setManager(manager) {
        this.manager = manager;
    }

    /**
     * Fonction qui attribue le rôle de l'utilisateur dans le kanban
     * @param role rôle de l'utilisateur dans le kanban
     */
    setRole(role) {
        this.role = role;
    }

    /**
     * Fonction qui initialise la liste des kanbans
     * @static
     * @param data objet généré par la réponse de la requête
     * @param filtre filtre sélectionné à appliquer 
     * @returns kanbans l'ensemble des kanbans (sous forme d'un tableau de UserKanbanModel)
     */
    static initKanbanList(data, filtre) {
        var kanbans = [];
        for (var i = 0; i < data.kanbans.length; ++i) {
            var currentKanban = data.kanbans[i];
            switch (filtre) {
                case 'manager':
                if(currentKanban.role === 'Gestionnaire') {
                    kanbans.push(new UserKanbanModel(currentKanban.kanbanId, currentKanban.name, currentKanban.status, currentKanban.manager, currentKanban.role));
                } 
                break;
                case 'guest':
                if(currentKanban.role === 'Invité' || currentKanban.role === 'Lecteur') {
                    kanbans.push(new UserKanbanModel(currentKanban.kanbanId, currentKanban.name, currentKanban.status, currentKanban.manager, currentKanban.role));
                }
                break;
                default:
                    kanbans.push(new UserKanbanModel(currentKanban.kanbanId, currentKanban.name, currentKanban.status, currentKanban.manager, currentKanban.role));
            }
        }
        return kanbans;
    }
}