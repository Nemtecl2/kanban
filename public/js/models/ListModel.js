/**
 * @class ListModel
 */
class ListModel {

    /**
     * Constructeur de la classe
     * @constructor
     * @param id identifiant de la liste
     * @param name nom de la liste
     * @param tasks ensemble des tâches de la liste
     */
    constructor(id, name, tasks) {
        this.id = id;
        this.name = name;
        this.tasks = tasks;
    }

    /**
     * Fonction qui attribue l'identifiant de la liste
     * @param id identifiant de la liste
     */
    setId(id) {
        this.id = id;
    }

    /**
     * Fonction qui attribue le nom de la liste
     * @param name nom de la liste
     */
    setName(name) {
        this.name = name;
    }

    /**
     * Fonction qui attribue l'ensemble des tâches de la liste
     * @param tasks tâches de la liste
     */
    setTask(tasks) {
        this.tasks = tasks;
    }
}