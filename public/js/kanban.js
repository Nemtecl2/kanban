var kanbanListTask;
var userRights;

$(document).ready(function () {

  // On récupère l'ID du kanban et de l'utilisateur
  var kanbanId = $('#kanban-id').val();
  initializeUserRights(kanbanId);

  // On créé le sélecteur de date avec le bon format
  $('#task-date-timepicker').datetimepicker({
    format: 'YYYY-MM-DD'
  });

  // Si le format entrée ne correspond pas à celui que l'on souhaite alors on efface
  $('#task-date-timepicker').on('change.datetimepicker', function (e) {
    if (!$('#task-date').val().match('^[0-9]')) {
      $('#task-date').val('');
    }
  });

  // Validation ajout d'utilisateurs
  $('#add-users-submit').on('click', function () {
    var users = $('#add-users').val();
    if (users.length === 0) {
      toastr.error('Veuillez saisir au moins un utilisateur', 'Erreur');
    } else {
      $.ajax({
        url: './ajax/insert_users_in_kanban.php',
        type: 'POST',
        dataType: 'json',
        data: {
          kanbanId: kanbanId,
          users: users
        },
        success: (data) => {
          if (data.success) {
            initializeAvailableUsers(kanbanId);
            toastr.success(data.message, 'Bravo');
          } else {
            toastr.error(data.message, 'Erreur');
          }
        }
      });
    }
  });

  // Validation ajout de listes
  $('#add-lists-submit').on('click', function () {
    var lists = $('#add-lists').val();
    if (lists.length === 0) {
      toastr.error('Veuillez saisir au moins une liste', 'Erreur');
    } else {
      $.ajax({
        url: './ajax/insert_lists.php',
        type: 'POST',
        dataType: 'json',
        data: {
          kanbanId: kanbanId,
          lists: lists
        },
        success: (data) => {
          if (data.success) {
            $("#add-lists").val(null).trigger('change');
            toastr.success('Liste(s) ajoutée(s)', 'Bravo');
            if (data.kanbanListTask.length !== 0) {
              kanbanListTask = KanbanModel.initKanban(data.kanbanListTask);
              displayKanban(kanbanListTask, userRights);
            }
          }
        }
      });
    }
  });

  // Validation ajout d'une tâche
  $('#add-task-submit').on('click', function () {
    var description = $('#add-task-description').html();
    var date = $('#add-task-date').val() !== '' ? $('#add-task-date').val() : null;
    var userId = $('#add-task-user').val() !== '' ? $('#add-task-user').val() : null;
    var listId = $('#add-task-list-id').val();
    if ($('#add-task-description').text().length === 0) {
      toastr.error('La taille de la description doit être supérieure à 0');
    } else {
      $.ajax({
        url: './ajax/insert_task.php',
        type: 'POST',
        dataType: 'json',
        data: {
          kanbanId: kanbanId,
          listId: listId,
          description: description,
          date: date,
          userId: userId,
        },
        success: (data) => {
          if (data.success) {
            $('#add-task-description').empty().trigger('change');
            $('#add-task-user').val(null).trigger('change');
            $('#add-task-date').val(null).trigger('change');
            toastr.success('Carte ajoutée', 'Bravo');
            if (data.kanbanListTask.length !== 0) {
              kanbanListTask = KanbanModel.initKanban(data.kanbanListTask);
              displayKanban(kanbanListTask, userRights);
            }
          }
        }
      });
    }
  });

  // Validation modification d'une liste
  $('#update-list-submit').on('click', function () {
    var listId = $('#current-list-id').val();
    var listName = $('#update-list-name').val();
    if (listName === '' || listName === null) {
      toastr.error('Veuillez saisir un nouveau nom pour la liste', 'Erreur');
    } else {
      $.ajax({
        url: './ajax/update_list.php',
        type: 'POST',
        dataType: 'json',
        data: {
          list: {
            id: listId,
            name: listName
          },
          kanbanId: kanbanId
        },
        success: (data) => {
          if (data.success) {
            $('#update-list-name').val('');
            toastr.success('Liste modifiée', 'Bravo');
            if (data.kanbanListTask.length !== 0) {
              kanbanListTask = KanbanModel.initKanban(data.kanbanListTask);
              displayKanban(kanbanListTask, userRights);
            }
          } else {
            toastr.error('La liste ' + listName + ' existe déjà', 'Erreur');
          }
        }
      });
    }
  });

  // Validation modification d'une tâche
  $('#change-task-submit').on('click', function () {
    var description = $('#change-task-description').html();
    var date = $('#change-task-date').val() !== '' ? $('#change-task-date').val() : null;
    var userId = $('#change-task-user').val() !== '' ? $('#change-task-user').val() : null;
    var taskId = $('#change-task-id').val();
    if ($('#change-task-description').text().length === 0) {
      toastr.error('La taille de la description doit être supérieure à 0');
    } else {
      $.ajax({
        url: './ajax/update_task.php',
        type: 'POST',
        dataType: 'json',
        data: {
          kanbanId: kanbanId,
          description: description,
          date: date,
          userId: userId,
          taskId: taskId
        },
        success: (data) => {
          if (data.success) {
            toastr.success('Carte modifiée', 'Bravo');
            if (data.kanbanListTask.length !== 0) {
              kanbanListTask = KanbanModel.initKanban(data.kanbanListTask);
              displayKanban(kanbanListTask, userRights);
            }
          }
        }
      });
    }
  });

  // Suppression d'une liste
  $('#delete-list').on('click', function () {
    var msgValidation = "Êtes-vous sûr de vouloir supprimer cette liste ?";
    openModalConfirm(msgValidation, 'list');
  });

  // Suppression d'une tâche
  $('#delete-task').on('click', function () {
    var msgValidation = "Êtes-vous sûr de vouloir supprimer cette tâche ?";
    openModalConfirm(msgValidation, 'task');
  });
});

/**
 * Fenêtre de confirmation
 * @param msgValidation le message apparaîssant dans la fenêtre de confirmation
 * @param mode chaîne de caractères correspond à une liste ou une tâche
 */
function openModalConfirm(msgValidation, mode) {
  $('#modalConfirm').modal({
    backdrop: 'static',
    keyboard: false
  })
  $('#modalConfirm').modal('show');
  $('#modalConfirm .modal-body').html(msgValidation);

  // Si l'utilisateur clique sur la croix ou sur Non
  $('#modalConfirm .close, #modalConfirm #noConfirmDialog').off('click').on('click', function () {
    $('#modalConfirm').modal('hide');
    // Ne rien faire
  });

  // Si l'utilisateur clique sur Oui
  $('#modalConfirm #yesConfirmDialog').off('click').on('click', function () {
    $('#modalConfirm').modal('hide');
    if (mode === 'list') {
      var listId = $('#current-list-id').val();
      $.ajax({
        url: './ajax/delete_list.php',
        type: 'POST',
        dataType: 'json',
        data: {
          listId: listId,
          kanbanId: $('#kanban-id').val()
        },
        success: (data) => {
          if (data.success) {
            toastr.success('Liste supprimée', 'Bravo');
            if (data.kanbanListTask.length !== 0) {
              kanbanListTask = KanbanModel.initKanban(data.kanbanListTask);
              displayKanban(kanbanListTask, userRights);
            } else {
              clearKanbanContainer();
            }
          }
        }
      });
    } else if (mode === 'task') {
      var userId = $('#change-task-user').val() !== '' ? $('#change-task-user').val() : null;
      var taskId = $('#change-task-id').val();
      var kanbanId = $('#kanban-id').val();
      $.ajax({
        url: './ajax/delete_task.php',
        type: 'POST',
        dataType: 'json',
        data: {
          taskId: taskId,
          kanbanId: kanbanId,
          userId: userId
        },
        success: (data) => {
          if (data.success) {
            toastr.success('Carte supprimée', 'Bravo');
            if (data.kanbanListTask.length !== 0) {
              kanbanListTask = KanbanModel.initKanban(data.kanbanListTask);
              displayKanban(kanbanListTask, userRights);
            }
          }
        }
      });
    }
  });
}

/**
 * Fonction qui récupère tous les utilisateurs d'un kanban
 * et appelle d'autres fonctions pour ajouter à la sélection
 * @param kanbanId identifiant du kanban
 */
function initializeAvailableUsers(kanbanId) {
  $.ajax({
    url: './ajax/select_available_users.php?kanbanId=' + kanbanId,
    type: 'GET',
    dataType: 'json',
    success: (data) => {
      if (data.success) {
        addListsToSelect();
        addUsersToSelect(data.users);
      }
    }
  });
}

/**
 * Fonction qui récupère toutes les informations nécessaire à un kanban (kanban, listes, tâches)
 * puis appelle la vue pour afficher le kanban
 * @param kanbanId identifiant du kanban
 */
function initializeKanbanListTask(kanbanId) {
  $.ajax({
    url: './ajax/select_kanban_list_task.php?kanbanId=' + kanbanId,
    type: 'GET',
    dataType: 'json',
    success: (data) => {
      if (data.success) {
        if (data.kanbanListTask.length !== 0) {
          kanbanListTask = KanbanModel.initKanban(data.kanbanListTask);
          displayKanban(kanbanListTask, userRights);
        }
      }
    }
  });
}

/**
 * Fonction qui récupère les droits de l'utilisateur
 * @param kanbanId identifiant du kanban
 */
function initializeUserRights(kanbanId) {
  $.ajax({
    url: './ajax/select_user_rights.php?kanbanId=' + kanbanId,
    type: 'GET',
    dataType: 'json',
    success: (data) => {
      if (data.success) {
        userRights = data.rights;
        initializeAvailableUsers(kanbanId);
        initializeKanbanListTask(kanbanId);
      }
    }
  });
}

/**
 * Fonction qui créé le glisser-déposer
 * @param userRights 
 */
function initializeDrag(userRights) {
  var lists = $('.kanban-list');

  var drakeArray = [];
  var scrollArray = [];

  scrollArray.push(window);
  scrollArray.push($('#kanban-container')[0]);

  lists.each(function () {
    var taskContainer = $(this).find('.task-container')[0];
    drakeArray.push(taskContainer);
    scrollArray.push(taskContainer);
  });

  var drake = dragula(drakeArray, {
    moves: function (el, source, handle, sibling) {
      var userId = $('#user-id').val();
      var taskUserId = $($(el).find('input:hidden')[0]).val();
      return ((userRights === 'manager' || userId === taskUserId) || (userRights === 'guest' && taskUserId === ''));
    }
  });

  // Lorsque la tâche est relâché
  drake.on('drop', function (el, target, source, sibling) {
    var taskId = $(el).attr('id').match(/\d+/)[0];
    var newListId = $(target).parent('.kanban-list').attr('id').match(/\d+/)[0];
    var kanbanId = $('#kanban-id').val();
    $.ajax({
      url: './ajax/update_task_list.php',
      type: 'POST',
      dataType: 'json',
      data: {
        taskId: taskId,
        kanbanId: kanbanId,
        listId: newListId
      },
      success: (data) => {
        if (data.success) {
          toastr.success(data.message, 'Bravo');
        } else {
          toastr.error(data.message, 'Erreur');
        }
      }
    });
  });

  // On créé le défilement automatique
  var scroll = autoScroll(scrollArray, {
    margin: 100,
    maxSpeed: 10,
    autoScroll: function () {
      return this.down && drake.dragging;
    }
  });
}

/**
 * Fonction qui récupère les membres du kanban pour les ajouter à la sélection (pour ajout d'une tâche)
 */
function initializeAvailableUsersByRole() {
  var kanbanId = $('#kanban-id').val();
  $.ajax({
    url: './ajax/select_available_users_by_role.php?kanbanId=' + kanbanId,
    type: 'GET',
    dataType: 'json',
    async: false,
    success: (data) => {
      if (data.success) {
        addTaskUsersToSelect(data.users);
      }
    }
  });
}