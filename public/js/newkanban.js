$(document).ready(function () {
    initializeUsers();

    $("form").submit(function (event) {
        createKanban();
        // Pour cancel le submit
        event.preventDefault();
    });
});

/**
 * Fonction qui récupère tous les utilisateurs sauf celui courant
 * et les ajoute à la liste déroulante de sélection d'utilisateurs
 */
function initializeUsers() {
    $.ajax({
        url: './ajax/select_users_except_one.php',
        type: 'GET',
        dataType: 'json',
        success: (data) => {
            var users = UserModel.initUserList(data);
            addUserToSelect(users);
        }
    });
}

/**
 * Fonction qui créé un kanban avec les données fournies dans le formulaire
 */
function createKanban() {
    var newKanban = new KanbanModel($('#name').val(), $('input[name=status]:checked').val(),
        $('#user-list').val(), $('#lists').val());
    $.ajax({
        url: './ajax/insert_kanban.php',
        type: 'POST',
        dataType: 'json',
        data: { kanban: newKanban },
        success: (data) => {
            if (data.success) {
                $("#new-kanban-form")[0].reset();
                $("#user-list").val(null).trigger('change');
                $("#lists").val(null).trigger('change');
                this.toastr.success(data.message, 'Bravo');
            } else {
                this.toastr.error(data.message, 'Erreur');
            }
        }
    })
}