$(document).ready(function () {
    initializeTasks();
});

// Fonction qui récupère les tâches puis appelle la vue pour l'affichage
function initializeTasks() {
    $.ajax({
        url: './ajax/select_tasks.php',
        type: 'GET',
        dataType: 'json',
        success: (data) => {
            var tasks = TaskModel.initTaskList(data);
            displayTasks(tasks, data.userId);
        }
    });
}

/**
 * Fonction qui rend la suppression possible
 */
function makeDeletionPossible() {
    $('#tasks tbody').on('click', '.delete-task', function () {
        var taskDescription = $($('.delete-task').parent().siblings('.task-description')[0]).text();
        var listName = $($('.delete-task').parent().siblings('.list-name')[0]).text();
        var kanbanName = $($('.delete-task').parent().siblings('.kanban-name')[0]).text();
        // On récupère l'id de la tâche, caché dans à l'intérieur de la description
        var taskId = $($('.delete-task').parent().siblings('.task-description')[0])
            .find('input[name=task-id]').val();

        var confirmMsg = 'Êtes-vous sûr de vouloir supprimer la tâche "' + taskDescription + '" de la liste "' +
            listName + '" du kanban "' + kanbanName + '" ?';

        modalConfirm(taskId, confirmMsg);
    });
}

/**
 * Fonction qui supprime une tâche
 * @param taskId identifiant de la tâche à supprimer 
 */
function deleteTask(taskId) {
    $.ajax({
        url: './ajax/delete_assigned_task.php',
        type: 'POST',
        dataType: 'json',
        data: {
            id: taskId
        },
        success: (data) => {
            if (data.success) {
                initializeTasks();
                this.toastr.success(data.message, 'Bravo');
            } else {
                this.toastr.error(data.message, 'Erreur');
            }
        }
    });
}