<?php

/**
 * Contrôleur pour la page du kanban
 */
session_start();
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
$isLogged = isset($_SESSION['user']);
$currentPage = "kanban";
require './models/KanbanModel.php';
require './models/UserKanbanModel.php';

// On vérifie les paramètres
if (isset($_GET['kanbanId'])) {
    // On récupère le kanban et son identifiant
    $kanban = KanbanModel::selectKanbanWithId($_GET['kanbanId']);
    $kanbanId = $_GET['kanbanId'];
    // Si le kanban existe
    if (count($kanban) == 1) {
        $name = $kanban[0]['Name'];
        $status = $kanban[0]['Status'];
        if ($isLogged) { // Si connecté
            // On récupère le rôle de l'utilisateur au sein du kanban
            $userKanban = UserKanbanModel::selectRole($_SESSION['user']['UserId'], $kanban[0]['KanbanId']);
            $userId = $_SESSION['user']['UserId'];
            // On vérifie que l'utilisateur peut accéder à ce kanban (inscrit ou public)
            if (count($userKanban)) {
                if ($userKanban[0]['Role'] === 'manager') { // Si l'utilisateur est gestionnaire
                    $canAdd = true;
                    $rights = "Tous les droits";
                    $isManager = true;
                } else {
                    $canAdd = false;
                    $rights = "Créer sa tâche, la supprimer et la déplacer";
                    $isManager = false;
                }
                // Renvoie la vue de la page du kanban
                require './views/html/KanbanView.php';
            } else if ($kanban[0]['Status'] === 'public') { // Connecté mais kanban public
                $canAdd = false;
                $rights = 'Lecture seule';
                $isManager = false;
                // Renvoie la vue de la page du kanban
                require './views/html/KanbanView.php';
            } else {
                $errorMessage = "Vous ne pouvez pas accéder à ce kanban";
                // Renvoie la vue de la page d'erreur
                require './views/html/ErrorView.php';
            }
        } else if ($kanban[0]['Status'] === 'public') { // Non connecté, donc on vérifie le statut du kanban
            $userId = -1;
            $canAdd = false;
            $rights = 'Lecture seule';
            $isManager = false;
            // Renvoie la vue de la page du kanban
            require './views/html/KanbanView.php';
        } else { // Non connecté et tentative d'accès à un kanban privé
            $errorMessage = "Vous ne pouvez pas accéder à ce kanban";
            // Renvoie la vue de la page d'erreur
            require './views/html/ErrorView.php';
        }
    } else { // Le kanban n'existe pas
        $errorMessage = "Veuillez sélectionner un kanban qui existe";
        // Renvoie la vue de la page d'erreur
        require './views/html/ErrorView.php';
    }
} else { // On essaye d'accéder à la page sans paramètre
    $errorMessage = "Veuillez sélectionner un kanban parmi les listes";
    // Renvoie la vue de la page d'erreur
    require './views/html/ErrorView.php';
}
?>