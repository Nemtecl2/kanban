<?php

/**
 * Contrôleur pour la page de configuration de la base de données
 */
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
require './models/InstallModel.php';

// On regarde si le formulaire a été envoyé
if (isset($_POST['install'])) {
    saveConfig($_POST['server'], $_POST['database'], $_POST['username'], $_POST['password'], isset($_POST['insert_data']));
} else {
    // Renvoie la vue de la page de configuration de la base de données
    require './views/html/InstallView.php';
}

/**
 * Fonction qui permet de sauvegarder la configuration de la base de données
 * @param server nom du serveur
 * @param db nom de la base de données
 * @param username nom de l'utilisateur
 * @param passwd mot de passe de l'utilisateur pour la base de données
 * @param insertData booléen valant true si l'utilisateur souhaite insérer des valeurs par défaut
 */
function saveConfig($server, $db, $username, $passwd, $insertData)
{
    // On vérifie que les variables ont bien été envoyées dans le formulaire
    if (!(empty($server) || empty($db) || empty($username) || empty($passwd))) {
        try {
            $dsn = 'mysql:host=' . $server . ';dbname=' . $db;
            $message = 'Les tables ont été créées';

            // On écrit dans le fichier config.txt
            file_put_contents('../config.txt', $dsn . '-' . $username . '-' . $passwd);

            // On initialise les tables
            UserModel::initTables();

            // Si l'utilisateur a décidé d'insérer des valeurs par défaut
            if ($insertData) {
                // On insère des données par défaut
                UserModel::insertData();
                $message = $message . ' et initialisées';
            }

            $success = true;
            // Renvoie la vue de la page de configuration de la base de données
            require './views/html/InstallView.php';
        } catch (Exception $e) {
            $success = false;
            $message = 'Veuillez remplir correctement les champs ou donner les droits nécessaires au dossier parent';
            // Renvoie la vue de la page de configuration de la base de données
            require './views/html/InstallView.php';
        }
    } else {
        // Si non, on redirige vers la vue avec un message d'erreur
        $success = false;
        $message = 'Veuillez remplir tous les champs';
        // Renvoie la vue de la page de configuration de la base de données
        require './views/html/InstallView.php';
    }
}
?>
