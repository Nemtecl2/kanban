<?php

/**
 * Contrôleur pour la page d'accueil
 */
session_start();
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
require './models/UserKanbanModel.php';
$currentPage = 'index';
$isLogged = isset($_SESSION['user']);

// Renvoie la vue de la page d'accueil
require './views/html/IndexView.php';
?>