# Présentation

Ce projet a été conçu dans le cadre d'un projet de notre matière Langage Web de première année de Master Génie de l'Informatique Logicielle.

# Notice d'installation

Cette partie explique comment utiliser ce projet et ce qu'il faut faire au préalable. 

Il y a plusieurs critères à respecter :
- Le projet a été développé avec l'environnement de la machine virtuelle, il faut donc utiliser exactement le même environnement que cette dernière pour être sûr que le projet fonctionne correctement. Selon le SGBD utilisé il est possible qu'il y ait des problèmes notamment sur les triggers qui ont été développés spécifiquement pour leur environnement ;
- Le dossier du projet doit être placé dans un serveur web (par exemple pour la VM il s'agit du répertoire /var/www/html) ;
- Une fois le projet placé dans le répertoire, il faut s'assurer d'avoir les droits nécessaires dans celui-ci puisque le fichier de configuration lié à la base de données se trouvera dans ce dossier. Il suffit donc d'effectuer les commandes suivantes : `sudo chmod -R  775 /var/www/html` puis `sudo chgrp -R  www-data /var/www/html/`.


Il faut ensuite accéder à la page install.php qui contiendra le formulaire permettant de créer les tables nécessaires au projet. Ce formulaire comporte les champs suivant :
- Nom du serveur ;
- Nom de la base de données ;
- Nom d'utilisateur ;
- Mot de passe.

# Seeding de la base

Il est également possible d'initialiser avec des données depuis une case à cocher. Voici ces  L'utilisateur toto avec comme mot de passe toto. Il est gestionnaire du kanban "mon premier kanban" et invité au kanban "mon second kanban" ;
- L'utilisateur tata avec comme mot de passe tata. Il est gestionnaire du kanban "mon second kanban" et invité au kanban "mon premier kanban" ; 
- L'utilisateur martin avec comme mot de passe test. Il est invité au kanban "wcn" ;
- L'utilisateur clem avec comme mot de passe test. Il est gestionnaire du kanban "wcn" ;
- L'utilisateur julien avec comme mot de passe test. Il est invité au kanban "wcn".

Une fois le formulaire validé, un fichier config.txt est créé au même niveau que le répertoire contenant le projet. Ce fichier contient une chaîne de caractères avec les différentes informations d'accès à la base de données. Il a été placé à ce niveau pour s'assurer qu'aucun utilisateur puisse y accéder depuis l'URL. Ce fichier sera lu à chaque fois que nous en avons besoin pour la création, la sélection, la modification et la suppression de données.

En fonction du rôle de l'utilisateur dans le kanban, il aura différentes permissions.

Il y a des tâches dans chacun des kanbans. Elles peuvent être attribuées à un utilisateur du kanban ou non et comporter une date ou non.

# Contributeurs
 - [Martin Blondel](https://gitlab.com/Philiippe) - martin.blondel@etu.univ-rouen.fr
 - [Clémént Drouin](https://gitlab.com/Nemtecl) - clement.drouin@etu.unvi-rouen.fr
 - [Julien Lamy](https://gitlab.com/nulji) - julien.lamy1@etu.univ-rouen.fr