<?php

/**
 * Contrôleur pour la page de configuration de compte
 */
session_start();
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
require './models/UserModel.php';
$currentPage = 'user';
$currentTab = 'changepseudo';
$isLogged = isset($_SESSION['user']);

if ($isLogged) { // Si connecté
    // Cas ou le formulaire est envoyé
    if (isset($_POST['type'])) {
        // On met à jour l'utilisateur
        updateUser($_POST['type']);
    } else {
        // Cas du clic sur l'un des onglets
        if (isset($_GET['tab'])) {
            $currentTab = changeTab($_GET['tab']);
        } else { // Cas du clic sur le pseudo depuis la navbar
            $currentTab = 'changepseudo';
        }
        // Renvoie la vue de la page de configuration de compte
        require './views/html/User/UserView.php';
    }
} else { // Tentative d'accès depuis l'URL sans être connecté
    $errorMessage = "Veuillez vous connecter pour accéder à cette page";
    // Renvoie la vue de la page d'erreur
    require './views/html/ErrorView.php';
}


// Fonction permettant de sélectionner l'onglet sélectionné 
function changeTab($selectedTab) {
    switch ($selectedTab) {
        case 'changepassword':
        case 'deleteaccount':
            $tab = $selectedTab;
            break;
        default:
            $tab = 'changepseudo';
            break;
    }
    return $tab;
}

// Fonction permettant d'appeler la bonne méthode du modèle
// selon le type de mofication (changer pseudo, changer mot de passe 
// ou supprimer compte)
function updateUser($type) {  
    $currentTab = changeTab($_POST['type']);
    global $currentPage, $sucess, $message, $isLogged;
    if ($type === 'changepseudo') {
        $username = $_SESSION['user']['Username'];
        UserModel::updatePseudo($username, $_POST['new_pseudo']);
        $user = UserModel::selectUser($username);
        $_SESSION['user'] = $user[0];
        $success = true;
        $message = 'Votre pseudo a bien été modifié';
        require './views/html/User/UserView.php';
    } else if ($type === 'changepassword') {
        if ($_POST['new_password'] === $_POST['confirm_password']) {
            if (password_verify($_POST['old_password'], $_SESSION['user']['Passwd'])) {
                $username = $_SESSION['user']['Username'];
                UserModel::updatePassword($username, $_POST['new_password']);
                $user = UserModel::selectUser($username, $_POST['old_password']);
                $_SESSION['user'] = $user[0];
                $success = true;
                $message = 'Votre mot de passe a bien été modifié';
            } else {
                $success = false;
                $message = 'Le mot de passe saisi est incorrect';
            }
        } else {
            $success = false;
            $message = 'Les mots de passe ne correspondent pas';
        }
        require './views/html/User/UserView.php';
    } else if ($type === 'deleteaccount') {
        if (password_verify($_POST['password'], $_SESSION['user']['Passwd'])) {
            $userId = $_SESSION['user']['UserId'];
            UserModel::deleteUser($userId);
            session_destroy();
            $_SESSION = [];
            $success = true;
            $message = 'Votre compte a bien été supprimé';
            $currentPage = 'index';
            $isLogged = isset($_SESSION['user']);
            require './views/html/IndexView.php';
        } else {
            $success = false;
            $message = 'Le mot de passe saisi est incorrect';
            require './views/html/User/UserView.php';
        }
    }
}
?>