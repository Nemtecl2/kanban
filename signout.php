<?php

/**
 * Contrôleur pour la page de déconnexion
 */
session_start();
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
require './models/UserModel.php';
$isLogged = isset($_SESSION['user']);

if ($isLogged) { // Si connecté
    session_destroy();
    $_SESSION = [];
    session_start();
    $isLogged = false;
    $success = true;
    $message = "Déconnexion réussie";
    $currentPage = 'index';
    // Renvoie la vue de la page d'accueil
    require './views/html/IndexView.php';
} else {
    $errorMessage = "Veuillez vous connecter pour accéder à cette page";
    // Renvoie la vue de la page d'erreur
    require './views/html/ErrorView.php';
}
?>