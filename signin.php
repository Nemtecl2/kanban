<?php

/**
 * Contrôleur pour la page de connexion
 */
session_start();
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
require './models/UserModel.php';
$currentPage = 'signin';
$isLogged = isset($_SESSION['user']);

if (!$isLogged) { // Si non connecté
    if (isset($_POST['username']) && isset($_POST['password'])) { // Si les informations sont saisies
        // On récupère l'utilisateur correspondant aux informations saisies
        $user = UserModel::selectUser($_POST['username']);
        if (count($user)) { // On vérifie qu'il existe
            if (password_verify($_POST['password'], $user[0]['Passwd'])) { // On vérifie le mot de passe
                $_SESSION['user'] = $user[0];
                $success = true;
                $message = "Connexion réussie";
                $isLogged = isset($_SESSION['user']);
                $currentPage = 'index';
                // Renvoie la vue de la page d'accueil
                require './views/html/IndexView.php';
            } else {
                $message = "Mot de passe incorrect";
                $success = false;
                // Renvoie la vue de la page de connexion
                require './views/html/SignInView.php';
            }
        } else {
            $message = "L\'utilisateur " . $_POST['username'] . " n\'existe pas";
            $success = false;
            // Renvoie la vue de la page de connexion
            require './views/html/SignInView.php';
        }
    } else {
        // Renvoie la vue de la page de connexion
        require './views/html/SignInView.php';
    }
} else {
    $errorMessage = "Vous êtes déjà connecté";
    // Renvoie la vue de la page d'erreur
    require './views/html/ErrorView.php';
}

?>