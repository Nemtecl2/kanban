INSERT INTO User (UserId, Username, Passwd, Pseudo) VALUES (NULL, 'toto', '$2y$10$0l1i2t7lE9.DXGkZ/aChz.Fp7z2D7FtEEAz7QGmiUR5ZAt.9k4ij6', 'Totolemagnifique');

INSERT INTO User (UserId, Username, Passwd, Pseudo) VALUES (NULL, 'tata', '$2y$10$Q9v4BbD9hAJqNl5iO/Wwcutgdu4phHmJZWoEHq3BmJimOfRjJ16cm', 'tatalaplusbelle');

INSERT INTO User (UserId, Username, Passwd, Pseudo) VALUES (NULL, 'martin', '$2y$10$5g7PkgG6w4qF./ph6KS6uuaBEkro.93vlqT819QPifG02QP6U.KBi', 'martin');

INSERT INTO User (UserId, Username, Passwd, Pseudo) VALUES (NULL, 'clem', '$2y$10$5g7PkgG6w4qF./ph6KS6uuaBEkro.93vlqT819QPifG02QP6U.KBi', 'clem');

INSERT INTO User (UserId, Username, Passwd, Pseudo) VALUES (NULL, 'julien', '$2y$10$5g7PkgG6w4qF./ph6KS6uuaBEkro.93vlqT819QPifG02QP6U.KBi', 'julien');

INSERT INTO Kanban (KanbanId, ManagerId, Name, Status) VALUES (1, 1, 'mon premier kanban', 'public');

INSERT INTO Kanban (KanbanId, ManagerId, Name, Status) VALUES (2, 2, 'mon second kanban', 'private');

INSERT INTO Kanban (KanbanId, ManagerId, Name, Status) VALUES (3, 4, 'wcn', 'private');

INSERT INTO UserKanban (UserKanbanId, UserId, KanbanId, Role) VALUES (NULL, 2, 1, 'guest');
 
INSERT INTO UserKanban (UserKanbanId, UserId, KanbanId, Role) VALUES (NULL, 1, 2, 'guest');

INSERT INTO UserKanban (UserKanbanId, UserId, KanbanId, Role) VALUES (NULL, 3, 3, 'guest');

INSERT INTO UserKanban (UserKanbanId, UserId, KanbanId, Role) VALUES (NULL, 5, 3, 'guest');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 1, 1, 'Voici ma première tache', '2018-12-25');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 1, 1, 'Faire ceci', '2018-12-15');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 2, NULL, 'Faire cela', '2018-12-14');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 2, 2, 'Ensuite ça', '2018-12-13');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 3, 2, 'Finir par ça', '2018-11-01');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 3, 1, 'Il y a aussi ça', '2019-01-01');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 3, 1, 'Et puis ça', '2019-02-14');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 3, 1, 'également ça', '2020-01-05');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 3, 1, 'mais aussi ça', '2019-11-21');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 2, 1, 'et ça', '2018-12-30');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 1, 1, 'ça aussi', '2018-12-29');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 1, 1, 'et pour finir ça', '2019-04-04');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 5, 3, 'pour martin', '2019-01-17');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 5, 4, 'pour clem', '2019-02-08');

INSERT INTO Task (TaskId, ListId, UserId, Description, Date) VALUES (NULL, 5, 5, 'pour julien', '2019-03-27');