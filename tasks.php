<?php

/**
 * Contrôleur pour la page des tâches affectées
 */
session_start();
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
require './models/TaskModel.php';
$currentPage = 'tasks';
$isLogged = isset($_SESSION['user']);

if ($isLogged) { // Si connecté
    // Renvoie la vue de la page des tâches affectées
    require './views/html/TasksView.php';
} else {
    $errorMessage = "Veuillez vous connecter pour accéder à cette page";
    // Renvoie la vue de la page d'erreur
    require './views/html/ErrorView.php';
}

?>