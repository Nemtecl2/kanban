<?php
/**
 * Contrôleur pour la page d'inscription
 */
session_start();
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
require './models/UserModel.php';
$currentPage = 'signup';
$isLogged = isset($_SESSION['user']);

if (!$isLogged) { // Si non connecté
    // On vérifie que les informations ont été saisies
    if (isset($_POST['pseudo']) && isset($_POST['username']) && isset($_POST['password']) && ($_POST['confirm_password'])) {
        $exists = count(UserModel::selectUser($_POST['username']));
        if ($exists) { // Si l'utilisateur existe
            $message = 'Un utilisateur ' . $_POST['username'] . ' existe déjà';
            $success = false;
            // Renvoie la vue de la page d'inscription
            require './views/html/SignUpView.php';
        } else {
            if ($_POST['password'] === $_POST['confirm_password']) { // Si les deux mots de passes saisies correspondent
                // On insère l'utilisateur
                UserModel::insertUser($_POST['username'], $_POST['password'], $_POST['pseudo']);
                $message = 'Votre compte a bien été créé';
                $success = true;
                // Renvoie la vue de la page d'inscription
                require './views/html/SignUpView.php';
            } else {
                $message = 'Les mots de passe ne correspondent pas';
                $success = false;
                // Renvoie la vue de la page d'inscription
                require './views/html/SignUpView.php';
            }
        }
    } else {
        // Renvoie la vue de la page d'inscription
        require './views/html/SignUpView.php';
    }
} else {
    $errorMessage = "Vous êtes déjà connecté";
    // Renvoie la vue de la page d'erreur
    require './views/html/ErrorView.php';
}
?>