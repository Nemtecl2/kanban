<?php

/**
 * Contrôleur pour la page d'ajout d'un kanban
 */
session_start();
$modelPath = './models/';
$scriptPath = './scripts/';
$configPath = '../';
$currentPage = 'newkanban';
$isLogged = isset($_SESSION['user']);

if ($isLogged) { // Si connecté
    // Renvoie la vue de la page d'ajout d'un kanban
    require './views/html/NewKanbanView.php';
} else {
    $errorMessage = "Veuillez vous connecter pour accéder à cette page";
    // Renvoie la vue de la page d'erreur
    require './views/html/ErrorView.php';
}
?>